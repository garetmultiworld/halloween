Event	ID	Name			Wwise Object Path	Notes
	14239492	Chest			\Default Work Unit\Chest	
	233434430	Fireplacestop			\Default Work Unit\Fireplacestop	
	278294116	Polilla			\Default Work Unit\Polilla	
	546945280	Box			\Default Work Unit\Box	
	554385050	Murcielago_stop			\Default Work Unit\Murcielago_stop	
	591946788	Musica3day			\Default Work Unit\Musica3day	
	608898761	Dungeon			\Default Work Unit\Dungeon	
	610905538	Patasola			\Default Work Unit\Patasola	
	745854206	MainScreenStop			\Default Work Unit\MainScreenStop	
	855542885	Stop_Musica_dungeon			\Default Work Unit\Stop_Musica_dungeon	
	896542383	Murcielago			\Default Work Unit\Murcielago	
	1138697795	EnterPortal			\Default Work Unit\EnterPortal	
	1206473638	Stopantorchas			\Default Work Unit\Stopantorchas	
	1404152342	Fireplace			\Default Work Unit\Fireplace	
	1412170784	PolillaIdel			\Default Work Unit\PolillaIdel	
	1590382621	SwitchB			\Default Work Unit\SwitchB	
	1590382622	SwitchA			\Default Work Unit\SwitchA	
	1684166838	PuertaDungeon			\Default Work Unit\PuertaDungeon	
	1862135557	NoMusic			\Default Work Unit\NoMusic	
	1935505266	Grande			\Default Work Unit\Grande	
	2009716632	stopexteriormusic			\Default Work Unit\stopexteriormusic	
	2056633052	Librorecetas			\Default Work Unit\Librorecetas	
	2332496055	Monsterulti			\Default Work Unit\Monsterulti	
	2337953912	Linterna			\Default Work Unit\Linterna	
	2378340940	Polillastop			\Default Work Unit\Polillastop	
	2784394907	Pueblomusic			\Default Work Unit\Pueblomusic	
	2961031941	Hyde			\Default Work Unit\Hyde	
	2993950537	Murcielagoidle			\Default Work Unit\Murcielagoidle	
	3026462669	Pasos			\Default Work Unit\Pasos	
	3264771457	Stopambience			\Default Work Unit\Stopambience	
	3274067216	Jinete			\Default Work Unit\Jinete	
	3658393046	MainScreen			\Default Work Unit\MainScreen	
	3675381839	EventTest			\Default Work Unit\EventTest	
	3842695823	Stone_switch			\Default Work Unit\Stone_switch	
	3983120477	Paramainmusic			\Default Work Unit\Paramainmusic	
	3991942870	Music			\Default Work Unit\Music	
	4006435971	Stopmusicacasa			\Default Work Unit\Stopmusicacasa	
	4095077922	trampa_lanzas			\Default Work Unit\trampa_lanzas	

Switch Group	ID	Name			Wwise Object Path	Notes
	1019184271	Ulti			\Default Work Unit\Ulti	
	3026462669	Pasos			\Default Work Unit\Pasos	
	3991942870	Music			\Default Work Unit\Music	

Switch	ID	Name	Switch Group			Notes
	610905538	Patasola	Ulti			
	1935505266	Grande	Ulti			
	2961031941	Hyde	Ulti			
	3274067216	Jinete	Ulti			
	1641284458	Bosque	Pasos			
	1926317228	Piedra	Pasos			
	2185786256	Gravel	Pasos			
	2306462273	Madera	Pasos			
	608898761	Dungeon	Music			
	2213992659	Exterior	Music			
	3543727709	CasaBruja	Music			

Trigger	ID	Name			Wwise Object Path	Notes
	2925122746	ChestStinger			\Default Work Unit\ChestStinger	

Effect plug-ins	ID	Name	Type				Notes
	484890516	Church_Small_Wood	Wwise RoomVerb			

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	15596770	Piano	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Casa Bruja 2\Piano_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\CasaBruja\Casa Bruja\Piano		279046
	22705607	libro resetas	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\libro resetas_3F75BDB9.wem		\Actor-Mixer Hierarchy\Default Work Unit\libro resetas		6802
	28008936	Pasos Piedra 1	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pasos piedra_0F456E72.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos Piedra\Pasos Piedra 1		1883
	31461839	Spell_General_Generic_Bright_Twinkly_Big-001	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Spell_General_Generic_Bright_Twinkly_Big-001_3F75BDB9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Spell_General_Generic_Bright_Twinkly_Big-001		40972
	49895168	Tremolo	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pueblo 2\Tremolo_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Pueblobarrasusto\Pueblo\Tremolo		507937
	52786008	Monstruo Grande	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Monstruo Grande_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Monster_Ulti\Monstruo Grande\Monstruo Grande		249739
	58629950	voz Bruja	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\exterior2\voz Bruja_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Exterior\exterior2\voz Bruja		261383
	86484835	cello fx	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Bounces\cello fx_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Dungeon\Main Loop\cello fx		623761
	86701614	Murcielagoidle_02	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Murcielagoidle_EFADA3D1.wem		\Actor-Mixer Hierarchy\Default Work Unit\Murcielagoidle\Murcielagoidle_02		3661
	100242965	Labs Strings	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Bounces\Labs Strings_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Dungeon\Main Loop\Labs Strings		335024
	104484884	Chest	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Stone Slide Sound Effects_39FD8F25.wem		\Actor-Mixer Hierarchy\Default Work Unit\Chest		13629
	137983026	Pasos madera 3	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pasos madera 2.0_79EF1E20.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos Madera\Pasos madera 3		4628
	175533723	violin harmonic	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Casa Bruja 2\violin harmonic_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\CasaBruja\Casa Bruja\violin harmonic		314735
	193143689	Db	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\exterior2\Db_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Exterior\exterior2\Db		589703
	197209363	trampa de lanzas variacion	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\trampa de lanzas variacion_3F75BDB9.wem		\Actor-Mixer Hierarchy\Default Work Unit\trampa de lanzas variacion		16794
	197319612	Celesta	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pueblo 2\Celesta_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Pueblobarrasusto\Pueblo\Celesta		811495
	204568693	Music Box	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Casa Bruja 2\Music Box_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\CasaBruja\Casa Bruja\Music Box		406101
	218329241	Pasos madera 1	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pasos madera 2.0_BE5F51C8.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos Madera\Pasos madera 1		4490
	226385022	violin pizz	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Casa Bruja 2\violin pizz_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\CasaBruja\Casa Bruja\violin pizz		404281
	286477635	Polillaidle_02	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\116188552-wing-flaps-giant-rapid-large-f.6pollilla idle_DF6A4FD7.wem		\Actor-Mixer Hierarchy\Default Work Unit\Polillaidle\Polillaidle_02		3280
	289282122	Choir	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pueblo 2\Choir_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Pueblobarrasusto\Pueblo\Choir		701004
	304656170	Voice Whispering	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Bounces\Voice Whispering_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Dungeon\Main Loop\Voice Whispering		514935
	326486645	cello 1	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Casa Bruja 2\cello 1_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\CasaBruja\Casa Bruja\cello 1		403455
	330796567	choir	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\exterior2\choir_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Exterior\exterior2\choir		372373
	333488934	Ambient synth	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pueblo 2\Ambient synth_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Pueblobarrasusto\Pueblo\Ambient synth		923860
	344771162	DoorKey	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\DoorKey_3F75BDB9.wem		\Actor-Mixer Hierarchy\Default Work Unit\DoorKey		9058
	392002666	Cluster and hair	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pueblo 2\Cluster and hair_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Pueblobarrasusto\Pueblo\Cluster and hair		860591
	404529886	Monster Ultis_patasola	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Monster Ultis_patasola_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Monster_Ulti\Patasola\Monster Ultis_patasola		287004
	419302124	Pasos Gravel 2	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pasos Gravel_F3327AF6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos Gravel\Pasos Gravel 2		6775
	434825131	celesta	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\exterior2\celesta_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Exterior\exterior2\celesta		638486
	437141849	Pasos Bosque 1	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pasos Bosque_CE7CF82E.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos Bosque\Pasos Bosque 1		5312
	458727022	Polillaidle_01	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\116188552-wing-flaps-giant-rapid-large-f.6pollilla idle_B31E7ED2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Polillaidle\Polillaidle_01		4343
	467744394	violin spiccato 	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pueblo 2\violin spiccato _9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Pueblobarrasusto\Pueblo\violin spiccato 		1093334
	510292504	Fireplace	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Fireplace.1_2A038581.wem		\Actor-Mixer Hierarchy\Default Work Unit\Fireplace		128733
	522774257	Harp	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Casa Bruja 2\Harp_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\CasaBruja\Casa Bruja\Harp		304676
	533260228	DB	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Casa Bruja 2\DB_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\CasaBruja\Casa Bruja\DB		400613
	556456622	Violin Harmonics	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Bounces\Violin Harmonics_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Dungeon\Main Loop\Violin Harmonics		618797
	566897447	Pasos Gravel 3	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pasos Gravel_DA11EAB4.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos Gravel\Pasos Gravel 3		6378
	568957852	Electric guitar	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pueblo 2\Electric guitar _9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Pueblobarrasusto\Pueblo\Electric guitar		949534
	592579543	Db Sfx	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pueblo 2\Db Sfx_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Pueblobarrasusto\Pueblo\Db Sfx		176241
	620070899	voz bruja	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pueblo 2\voz bruja_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Pueblobarrasusto\Pueblo\voz bruja		224765
	628714509	Caja Rompiendose	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Box Break_52E1ED2F.wem		\Actor-Mixer Hierarchy\Default Work Unit\Caja Rompiendose		14469
	632081582	Music Box	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Bounces\Music Box_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Dungeon\Main Loop\Music Box		601094
	646495409	Monster Ultis_Jinete	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Monster Ultis_Jinete_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Monster_Ulti\Jinete\Monster Ultis_Jinete		173789
	647905123	Db Fx	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\exterior2\Db Fx_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Exterior\exterior2\Db Fx		257846
	655909858	EnterPortal	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Screen wosh 2_3F75BDB9.wem		\Actor-Mixer Hierarchy\Default Work Unit\EnterPortal		23480
	658048220	Ambience synth	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Bounces\Ambience synth_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Dungeon\Main Loop\Ambience synth		610815
	668548626	Music Box	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pueblo 2\Music Box_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Pueblobarrasusto\Pueblo\Music Box		1017551
	687739504	Polillaidle	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\116188552-wing-flaps-giant-rapid-large-f.6pollilla idle_14C3479F.wem		\Actor-Mixer Hierarchy\Default Work Unit\Polillaidle\Polillaidle		5501
	704080304	Pasos Piedra 2	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pasos piedra_AFBE4A48.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos Piedra\Pasos Piedra 2		1833
	704226616	Harp	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pueblo 2\Harp_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Pueblobarrasusto\Pueblo\Harp		394421
	745673585	Stone switch	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Stone switch_3F75BDB9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Stone switch		9369
	748628481	Murcielagoidle	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Murcielagoidle_035C75FC.wem		\Actor-Mixer Hierarchy\Default Work Unit\Murcielagoidle\Murcielagoidle		3617
	776985046	Pasos Bosque 2	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pasos Bosque_B4C8C936.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos Bosque\Pasos Bosque 2		5242
	803160291	Labs strings	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\exterior2\Labs strings_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Exterior\exterior2\Labs strings		630587
	805427827	String sfx	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\exterior2\String sfx_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Exterior\exterior2\String sfx		632853
	817707789	cello spiccato	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pueblo 2\cello spiccato_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Pueblobarrasusto\Pueblo\cello spiccato		859533
	824571470	Polilla	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Polilla_AC4B6E69.wem		\Actor-Mixer Hierarchy\Default Work Unit\Polilla		158230
	829120364	Pasos Bosque 3	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pasos Bosque_9DD425C8.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos Bosque\Pasos Bosque 3		6505
	839994160	Monster Ultis_ J&H	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Monster Ultis_ J&H_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Monster_Ulti\J&H\Monster Ultis_ J&H		499906
	842547776	Stinger	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Stinger_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\stingers\Stinger\Stinger		68114
	846162499	cello 1	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Casa Bruja 2\cello 2_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\CasaBruja\Casa Bruja\cello 1		397626
	875906388	Tremolo	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Casa Bruja 2\Tremolo_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\CasaBruja\Casa Bruja\Tremolo		267430
	876144628	music box	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\exterior2\music box_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Exterior\exterior2\music box		425793
	881444560	voz  bruja	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Bounces\voz  bruja_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Dungeon\Main Loop\voz  bruja		350531
	891639641	Pasos Piedra 3	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pasos piedra_1A329FA3.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos Piedra\Pasos Piedra 3		1605
	899486548	Tremolo	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\exterior2\Tremolo_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Exterior\exterior2\Tremolo		390457
	912967427	Murcielagoidle_01	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Murcielagoidle_F2889E75.wem		\Actor-Mixer Hierarchy\Default Work Unit\Murcielagoidle\Murcielagoidle_01		4012
	928147192	cello sfx	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Casa Bruja 2\cello sfx_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\CasaBruja\Casa Bruja\cello sfx		362671
	935158673	violin harmonics	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pueblo 2\violin harmonics_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Pueblobarrasusto\Pueblo\violin harmonics		963437
	942104435	Drum	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pueblo 2\Drum_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Pueblobarrasusto\Pueblo\Drum		234610
	943360994	Db	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pueblo 2\Db_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Pueblobarrasusto\Pueblo\Db		440368
	958521796	Pasos madera 2	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pasos madera 2.0_10728F9C.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos Madera\Pasos madera 2		4337
	963447476	harpsichord	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\exterior2\harpsichord_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Exterior\exterior2\harpsichord		673894
	1003343889	DB  fx	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Bounces\DB  fx_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Dungeon\Main Loop\DB  fx		221250
	1047196531	Murcielago	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\flying bats sound effects.3_1_5AD50F7A.wem		\Actor-Mixer Hierarchy\Default Work Unit\Murcielago		121479
	1069102693	Pasos Gravel 1	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pasos Gravel_01434FCF.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos Gravel\Pasos Gravel 1		5863

