Event	ID	Name			Wwise Object Path	Notes
	1862135557	NoMusic			\Default Work Unit\NoMusic	
	2056633052	Librorecetas			\Default Work Unit\Librorecetas	
	2337953912	Linterna			\Default Work Unit\Linterna	
	3026462669	Pasos			\Default Work Unit\Pasos	
	3340632179	Caldero			\Default Work Unit\Caldero	
	3831537568	Puerta			\Default Work Unit\Puerta	
	3991942870	Music			\Default Work Unit\Music	

Switch Group	ID	Name			Wwise Object Path	Notes
	3026462669	Pasos			\Default Work Unit\Pasos	
	3991942870	Music			\Default Work Unit\Music	

Switch	ID	Name	Switch Group			Notes
	1641284458	Bosque	Pasos			
	1926317228	Piedra	Pasos			
	2185786256	Gravel	Pasos			
	2306462273	Madera	Pasos			
	608898761	Dungeon	Music			
	2213992659	Exterior	Music			
	3543727709	CasaBruja	Music			

Trigger	ID	Name			Wwise Object Path	Notes
	2925122746	ChestStinger			\Default Work Unit\ChestStinger	

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	15596770	Piano	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Casa Bruja 2\Piano_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\CasaBruja\Casa Bruja\Piano		279046
	22705607	libro resetas	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\libro resetas_3F75BDB9.wem		\Actor-Mixer Hierarchy\Default Work Unit\libro resetas		6802
	28008936	Pasos Piedra 1	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pasos piedra_0F456E72.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos Piedra\Pasos Piedra 1		1883
	31461839	Spell_General_Generic_Bright_Twinkly_Big-001	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Spell_General_Generic_Bright_Twinkly_Big-001_3F75BDB9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Spell_General_Generic_Bright_Twinkly_Big-001		40972
	58629950	voz Bruja	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\exterior2\voz Bruja_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Exterior\exterior2\voz Bruja		261383
	86484835	cello fx	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Bounces\cello fx_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Dungeon\Main Loop\cello fx		623761
	100242965	Labs Strings	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Bounces\Labs Strings_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Dungeon\Main Loop\Labs Strings		335024
	137983026	Pasos madera 3	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pasos madera 2.0_79EF1E20.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos Madera\Pasos madera 3		4628
	175533723	violin harmonic	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Casa Bruja 2\violin harmonic_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\CasaBruja\Casa Bruja\violin harmonic		314735
	193143689	Db	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\exterior2\Db_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Exterior\exterior2\Db		589703
	204568693	Music Box	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Casa Bruja 2\Music Box_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\CasaBruja\Casa Bruja\Music Box		406101
	218329241	Pasos madera 1	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pasos madera 2.0_BE5F51C8.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos Madera\Pasos madera 1		4490
	226385022	violin pizz	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Casa Bruja 2\violin pizz_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\CasaBruja\Casa Bruja\violin pizz		404281
	304656170	Voice Whispering	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Bounces\Voice Whispering_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Dungeon\Main Loop\Voice Whispering		514935
	326486645	cello 1	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Casa Bruja 2\cello 1_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\CasaBruja\Casa Bruja\cello 1		403455
	330796567	choir	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\exterior2\choir_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Exterior\exterior2\choir		372373
	419302124	Pasos Gravel 2	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pasos Gravel_F3327AF6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos Gravel\Pasos Gravel 2		6775
	434825131	celesta	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\exterior2\celesta_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Exterior\exterior2\celesta		638486
	437141849	Pasos Bosque 1	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pasos Bosque_CE7CF82E.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos Bosque\Pasos Bosque 1		5312
	480688551	Puerta chirriando 2	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Puerta chirriando _CC83C626.wem		\Actor-Mixer Hierarchy\Default Work Unit\Puerta Chirriando\Puerta chirriando 2		34441
	522774257	Harp	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Casa Bruja 2\Harp_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\CasaBruja\Casa Bruja\Harp		304676
	533260228	DB	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Casa Bruja 2\DB_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\CasaBruja\Casa Bruja\DB		400613
	556456622	Violin Harmonics	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Bounces\Violin Harmonics_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Dungeon\Main Loop\Violin Harmonics		618797
	566897447	Pasos Gravel 3	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pasos Gravel_DA11EAB4.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos Gravel\Pasos Gravel 3		6378
	632081582	Music Box	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Bounces\Music Box_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Dungeon\Main Loop\Music Box		601094
	647905123	Db Fx	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\exterior2\Db Fx_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Exterior\exterior2\Db Fx		257846
	658048220	Ambience synth	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Bounces\Ambience synth_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Dungeon\Main Loop\Ambience synth		610815
	704080304	Pasos Piedra 2	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pasos piedra_AFBE4A48.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos Piedra\Pasos Piedra 2		1833
	776985046	Pasos Bosque 2	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pasos Bosque_B4C8C936.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos Bosque\Pasos Bosque 2		5242
	803160291	Labs strings	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\exterior2\Labs strings_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Exterior\exterior2\Labs strings		630587
	805427827	String sfx	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\exterior2\String sfx_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Exterior\exterior2\String sfx		632853
	829120364	Pasos Bosque 3	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pasos Bosque_9DD425C8.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos Bosque\Pasos Bosque 3		6505
	842547776	Stinger	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Stinger_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\stingers\Stinger\Stinger		68114
	846162499	cello 1	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Casa Bruja 2\cello 2_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\CasaBruja\Casa Bruja\cello 1		397626
	875906388	Tremolo	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Casa Bruja 2\Tremolo_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\CasaBruja\Casa Bruja\Tremolo		267430
	876144628	music box	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\exterior2\music box_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Exterior\exterior2\music box		425793
	881444560	voz  bruja	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Bounces\voz  bruja_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Dungeon\Main Loop\voz  bruja		350531
	891639641	Pasos Piedra 3	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pasos piedra_1A329FA3.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos Piedra\Pasos Piedra 3		1605
	899486548	Tremolo	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\exterior2\Tremolo_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Exterior\exterior2\Tremolo		390457
	928147192	cello sfx	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Casa Bruja 2\cello sfx_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\CasaBruja\Casa Bruja\cello sfx		362671
	958521796	Pasos madera 2	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pasos madera 2.0_10728F9C.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos Madera\Pasos madera 2		4337
	963447476	harpsichord	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\exterior2\harpsichord_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Exterior\exterior2\harpsichord		673894
	997165245	Puerta chirriando 3	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Puerta chirriando _B15F60F2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Puerta Chirriando\Puerta chirriando 3		48600
	1003343889	DB  fx	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Bounces\DB  fx_9EDFC787.wem		\Interactive Music Hierarchy\Default Work Unit\Music\Dungeon\Main Loop\DB  fx		221250
	1031935644	Cáldero	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Cáldero_51DC12EA.wem		\Actor-Mixer Hierarchy\Default Work Unit\Cáldero		503345
	1032670527	Puerta chirriando 1	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Puerta chirriando _44319C82.wem		\Actor-Mixer Hierarchy\Default Work Unit\Puerta Chirriando\Puerta chirriando 1		25620
	1069102693	Pasos Gravel 1	Y:\Documents\halloween\halloween_WwiseProject\.cache\Android\SFX\Pasos Gravel_01434FCF.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos Gravel\Pasos Gravel 1		5863

