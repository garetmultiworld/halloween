/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID BOX = 546945280U;
        static const AkUniqueID CALDERO = 3340632179U;
        static const AkUniqueID CHEST = 14239492U;
        static const AkUniqueID DIA3STATE = 2447119567U;
        static const AkUniqueID DUNGEON = 608898761U;
        static const AkUniqueID ENTERPORTAL = 1138697795U;
        static const AkUniqueID EVENTTEST = 3675381839U;
        static const AkUniqueID FIREPLACE = 1404152342U;
        static const AkUniqueID FIREPLACESTOP = 233434430U;
        static const AkUniqueID GRANDE = 1935505266U;
        static const AkUniqueID HYDE = 2961031941U;
        static const AkUniqueID JINETE = 3274067216U;
        static const AkUniqueID LIBRORECETAS = 2056633052U;
        static const AkUniqueID LINTERNA = 2337953912U;
        static const AkUniqueID MAINSCREEN = 3658393046U;
        static const AkUniqueID MAINSCREENSTOP = 745854206U;
        static const AkUniqueID MONSTERULTI = 2332496055U;
        static const AkUniqueID MURCIELAGO = 896542383U;
        static const AkUniqueID MURCIELAGO_STOP = 554385050U;
        static const AkUniqueID MURCIELAGOIDLE = 2993950537U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID MUSIC_STATE = 3826569560U;
        static const AkUniqueID MUSICA3DAY = 591946788U;
        static const AkUniqueID NIGHTAMBIENT = 128157627U;
        static const AkUniqueID NOMUSIC = 1862135557U;
        static const AkUniqueID PARAMAINMUSIC = 3983120477U;
        static const AkUniqueID PASOS = 3026462669U;
        static const AkUniqueID PATASOLA = 610905538U;
        static const AkUniqueID POLILLA = 278294116U;
        static const AkUniqueID POLILLAIDEL = 1412170784U;
        static const AkUniqueID POLILLASTOP = 2378340940U;
        static const AkUniqueID PUEBLOCONANTORCHA = 785810316U;
        static const AkUniqueID PUEBLOMUSIC = 2784394907U;
        static const AkUniqueID PUERTA = 3831537568U;
        static const AkUniqueID PUERTADUNGEON = 1684166838U;
        static const AkUniqueID STONE_SWITCH = 3842695823U;
        static const AkUniqueID STOP_MUSICA_DUNGEON = 855542885U;
        static const AkUniqueID STOPAMBIENCE = 3264771457U;
        static const AkUniqueID STOPANTORCHAS = 1206473638U;
        static const AkUniqueID STOPEXTERIORMUSIC = 2009716632U;
        static const AkUniqueID STOPMUSICACASA = 4006435971U;
        static const AkUniqueID SWITCHA = 1590382622U;
        static const AkUniqueID SWITCHB = 1590382621U;
        static const AkUniqueID TRAMPA_LANZAS = 4095077922U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace MUSIC
        {
            static const AkUniqueID GROUP = 3991942870U;

            namespace STATE
            {
                static const AkUniqueID CASABRUJA = 3543727709U;
                static const AkUniqueID DIA_3 = 97531999U;
                static const AkUniqueID DUNGEON = 608898761U;
                static const AkUniqueID EXTERIOR = 2213992659U;
                static const AkUniqueID MAIN_MENU = 2005704188U;
                static const AkUniqueID NONE = 748895195U;
            } // namespace STATE
        } // namespace MUSIC

    } // namespace STATES

    namespace SWITCHES
    {
        namespace MUSIC
        {
            static const AkUniqueID GROUP = 3991942870U;

            namespace SWITCH
            {
                static const AkUniqueID CASABRUJA = 3543727709U;
                static const AkUniqueID DUNGEON = 608898761U;
                static const AkUniqueID EXTERIOR = 2213992659U;
            } // namespace SWITCH
        } // namespace MUSIC

        namespace PASOS
        {
            static const AkUniqueID GROUP = 3026462669U;

            namespace SWITCH
            {
                static const AkUniqueID BOSQUE = 1641284458U;
                static const AkUniqueID GRAVEL = 2185786256U;
                static const AkUniqueID MADERA = 2306462273U;
                static const AkUniqueID PIEDRA = 1926317228U;
            } // namespace SWITCH
        } // namespace PASOS

        namespace ULTI
        {
            static const AkUniqueID GROUP = 1019184271U;

            namespace SWITCH
            {
                static const AkUniqueID GRANDE = 1935505266U;
                static const AkUniqueID HYDE = 2961031941U;
                static const AkUniqueID JINETE = 3274067216U;
                static const AkUniqueID PATASOLA = 610905538U;
            } // namespace SWITCH
        } // namespace ULTI

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID MASTERVOLUME = 2918011349U;
        static const AkUniqueID MUSICVOLUME = 2346531308U;
        static const AkUniqueID SFXVOLUME = 988953028U;
        static const AkUniqueID ULTI = 1019184271U;
    } // namespace GAME_PARAMETERS

    namespace TRIGGERS
    {
        static const AkUniqueID CHESTSTINGER = 2925122746U;
    } // namespace TRIGGERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID CASABRUJA = 3543727709U;
        static const AkUniqueID HALLOWEEN = 804732334U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID TOWN = 3091570009U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID SFX = 393239870U;
        static const AkUniqueID ULTI = 1019184271U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID NUEVO = 1093970598U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
