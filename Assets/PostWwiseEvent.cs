﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PostWwiseEvent : MonoBehaviour
{   
    public AK.Wwise.Event MyEvent;
    // Start is called before the first frame update
    public void PlayBoxCrash()
    {
       MyEvent.Post(gameObject);
    }
    public void PlayChestOpen()
     {
       MyEvent.Post(gameObject);
    }
     public void PlayMurcielagoidle()
     {
       MyEvent.Post(gameObject);
    }
    
    public void PlayPolillaidle()
    {
       MyEvent.Post(gameObject);
    }
    public void Playdoor()
    {
       MyEvent.Post(gameObject);
    }
    public void PlayHeal()
    {
       MyEvent.Post(gameObject);
    }
    public void PlayExplosion()
    {
       MyEvent.Post(gameObject);
    }
}
