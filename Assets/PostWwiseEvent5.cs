﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PostWwiseEvent5 : MonoBehaviour
{
     public AK.Wwise.Event Musica;
    // Start is called before the first frame update
    void Start()
    {
        Musica.Post(gameObject);
      
    }
  
    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "CasaBruja")
        {
            AkSoundEngine.SetSwitch("Music", "CasaBruja", gameObject);
        }
        if (collider.gameObject.tag == "Exterior")
        {
            AkSoundEngine.SetSwitch("Music", "Exterior", gameObject);
        }
    }  
}
