﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Riot : TriggerInterface
{

    protected List<GameObject> TinyRioters=new List<GameObject>();
    protected List<GameObject> SmallRioters = new List<GameObject>();
    protected List<GameObject> MediumRioters = new List<GameObject>();
    protected List<GameObject> BigRioters = new List<GameObject>();

    protected float TotalRioters;

    protected GenericEventListener _spookChanged;

    void Awake()
    {
        _spookChanged = gameObject.AddComponent(typeof(GenericEventListener)) as GenericEventListener;
        _spookChanged.EventName = "SpookChanged";
        _spookChanged.OnEvent = this;
        _spookChanged.Register();
    }

    public override void Cancel()
    {
        
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        float alivePercent=(100f-BasicPlayerController.instance.GetSpook())/100f;
        float aliveRioters = alivePercent * TotalRioters;
        while (aliveRioters > 1)
        {
            if(aliveRioters >= 4&& BigRioters.Count>0)
            {
                aliveRioters -= 4;
                Destroy(BigRioters[0]);
                BigRioters.RemoveAt(0);
            }
            else if (aliveRioters >= 3 && MediumRioters.Count > 0)
            {
                aliveRioters -= 3;
                Destroy(MediumRioters[0]);
                MediumRioters.RemoveAt(0);
            }
            else if (aliveRioters >= 2 && SmallRioters.Count > 0)
            {
                aliveRioters -= 2;
                Destroy(SmallRioters[0]);
                SmallRioters.RemoveAt(0);
            }
            else
            {
                aliveRioters -= 1;
                Destroy(TinyRioters[0]);
                TinyRioters.RemoveAt(0);
            }
        }
    }

    protected void CalculateRioters()
    {

    }

    void Start()
    {
        GameObject go;
        foreach (Transform child in transform)
        {
            go = child.gameObject;
            if (go.name.StartsWith("Tiny_Riot"))
            {
                TinyRioters.Add(go);
            }
            else if (go.name.StartsWith("Small_Riot"))
            {
                SmallRioters.Add(go);
            }
            else if (go.name.StartsWith("Medium_Riot"))
            {
                MediumRioters.Add(go);
            }
            else if (go.name.StartsWith("Big_Riot"))
            {
                BigRioters.Add(go);
            }
        }
        TotalRioters = TinyRioters.Count + (SmallRioters.Count * 2) + (MediumRioters.Count * 3) + (BigRioters.Count * 4);
    }

}
