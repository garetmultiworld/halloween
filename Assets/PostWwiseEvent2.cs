﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PostWwiseEvent2 : MonoBehaviour
{    
    public AK.Wwise.Event Footsteps;
    // Start is called before the first frame update
    public void Playfootsteps()
    {
    Footsteps.Post(gameObject);
    
    }
  
    void OnTriggerEnter2D(Collider2D collider)
    {  
        if (collider.gameObject.tag == "Madera")
        {
            AkSoundEngine.SetSwitch("Pasos", "Madera", gameObject);
        }
        if (collider.gameObject.tag == "Gravel")
        {
            AkSoundEngine.SetSwitch("Pasos", "Gravel", gameObject);
        }
         if (collider.gameObject.tag == "Piedra")
        {
            AkSoundEngine.SetSwitch("Pasos", "Piedra", gameObject);
        }
         if (collider.gameObject.tag == "Bosque")
        {
            AkSoundEngine.SetSwitch("Pasos", "Bosque", gameObject);
        }
    }
     
    
    // Update is called once per frame
    void Update()
    {
        
    }
}
