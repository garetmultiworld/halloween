﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionSound : MonoBehaviour
{
    public AK.Wwise.Event Sound;

    void PlayExplosionSound()
    {
        Sound.Post(gameObject);
    }
}
