﻿using UnityEngine;
using UnityEngine.UI;

public class volumechange : MonoBehaviour
{
    public string whatValue;
    public Slider thisSlider;
    public float masterVolume;
    public float musicVolume;
    public float sfxVolume;
    
    public void SetSpecificVolume()
    {
        float sliderValue = thisSlider.value;
        if (whatValue == "Master")
        {
            masterVolume = sliderValue;
            PersistanceManager.Instance.SetMasterVolume(sliderValue);
            AkSoundEngine.SetRTPCValue("MasterVolume", sliderValue);
        }
        if (whatValue == "Music")
        {
            musicVolume = sliderValue;
            PersistanceManager.Instance.SetMusicVolume(sliderValue);
            AkSoundEngine.SetRTPCValue("MusicVolume", sliderValue);
        }
        if (whatValue == "Sfx")
        {
            sfxVolume = thisSlider.value;
            PersistanceManager.Instance.SetSfxVolume(sliderValue);
            AkSoundEngine.SetRTPCValue("SfxVolume", sliderValue);
        }
    }

    public void LoadVolume()
    {
        SoundData soundData = PersistanceManager.Instance.GetSoundData();
        switch (whatValue)
        {
            case "Master":
                masterVolume= soundData.masterVolume;
                thisSlider.value = soundData.masterVolume;
                break;
            case "Music":
                musicVolume = soundData.musicVolume;
                thisSlider.value = soundData.musicVolume;
                break;
            case "Sfx":
                sfxVolume = soundData.sfxVolume;
                thisSlider.value = soundData.sfxVolume;
                break;
        }
    }
       
}
