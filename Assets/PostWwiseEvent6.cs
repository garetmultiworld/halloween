﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PostWwiseEvent6 : MonoBehaviour
{    
    public string initialTag;
    public AK.Wwise.Event Monster;
    // Start is called before the first frame update
    void Start()
    {
        Monster.Post(gameObject);
        SetTag(initialTag);
    }

    void SetTag(string newTag)
    {
        if (newTag == "Grande")
        {
            AkSoundEngine.SetSwitch("Ulti", "Grande", gameObject);
        }
        if (newTag == "Hyde")
        {
            AkSoundEngine.SetSwitch("Ulti", "Hyde", gameObject);
        }
        if(newTag == "Jinete")
        {
            AkSoundEngine.SetSwitch("Ulti", "Jinete", gameObject);
        }
        if(newTag == "Patasola")
        {
            AkSoundEngine.SetSwitch("Ulti", "Patasola", gameObject);
        }
    }
   
}
