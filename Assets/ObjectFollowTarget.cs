﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectFollowTarget : MonoBehaviour
{
    public GameObject player;        //Public variable to store a reference to the player game object
    private Vector3 CameraPosition;

    //Private variable to store the offset distance between the player and camera

    // Use this for initialization
    void Start()
    {
        CameraPosition = new Vector3(player.transform.position.x, player.transform.position.y, 0);
       transform.position = CameraPosition;
        
    }

    // LateUpdate is called after Update each frame
    void LateUpdate()
    {
        CameraPosition = new Vector3(player.transform.position.x, player.transform.position.y, 0);
        transform.position = CameraPosition;
    }
}

