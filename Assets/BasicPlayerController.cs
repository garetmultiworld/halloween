using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class BasicPlayerController : MonoBehaviour
{

    public enum Actions
    {
        Interact,
        StaffLight
    }

    public enum Events
    {
        DisableMovement,
        EnableMovement
    }

    public static BasicPlayerController instance;
    public TriggerInterface InteractTrigger;
    public float InteractRadius=20;
    public float InteractDelay = 1;
    public string StaffLightInput = "Fire2";
    public float StaffLightDelay = 1;
    public TriggerInterface StaffLightTrigger;
    public TriggerInterface OnFullSpookTrigger;
    public TriggerInterface OnSpookBarAnimationFinish;
    public TriggerInterface OnFootstep;
    public Joystick joystick;
    public float speed;
    public Animator animator;
    public Slider SpookSlider;
    public float SpookBarFillTime=4;
    public AnimationCurve SpookBarAnimCurve;
    public Inventory inventory;

    private Rigidbody2D _body;
    private float _initialScaleX;
    private float _spookAmount = 0;
    private float _animatingSpookBarTimer;
    private bool _animatingSpookBar = false;
    private bool _movementAllowed = true;
    private float _startingSpookValue;
    private float _interactTimer=0;
    private float _staffLightTimer = 0;

    BasicPlayerController()
    {
        instance = this;
        _interactTimer = InteractDelay;
    }

    public void OnCharacterStep()
    {
        if (OnFootstep != null)
        {
            OnFootstep.Trigger();
        }
    }

    public void AddSpook(float amount)
    {
        _animatingSpookBarTimer = 0;
        _animatingSpookBar = true;
        _startingSpookValue = _spookAmount;
        _spookAmount += amount;
    }

    public void InitSpookFeedback()
    {
        StartCoroutine(SetSpookSliderValue());
    }

    private IEnumerator SetSpookSliderValue()
    {
        while (_animatingSpookBar)
        {
            float t = _animatingSpookBarTimer / SpookBarFillTime;
            float newValue = _startingSpookValue + ((_spookAmount - _startingSpookValue) * SpookBarAnimCurve.Evaluate(t));
            _animatingSpookBarTimer += Time.deltaTime;
            if (_animatingSpookBarTimer >= SpookBarFillTime)
            {
                SpookSlider.value = _spookAmount;
                _animatingSpookBar = false;
                if (OnSpookBarAnimationFinish != null)
                {
                    OnSpookBarAnimationFinish.Trigger();
                }
            }
            else
            {
                SpookSlider.value = newValue;
            }
            yield return null;
        }
        GenericEventManager.Instance.TriggerStringEvent(gameObject, "SpookChanged", "", 0);
        if (_spookAmount >= 100)
        {
            _spookAmount = 100;
            if (OnFullSpookTrigger != null)
            {
                OnFullSpookTrigger.Trigger();
            }
        }
    }

    public float GetSpook()
    {
        return _spookAmount;
    }

    public void PerformAction(Actions action)
    {
        switch (action)
        {
            case Actions.Interact:
                Collider2D[] hitColliders = Physics2D.OverlapCircleAll(new Vector2(transform.position.x, transform.position.y), InteractRadius);
                Interactable interactable;
                foreach (Collider2D hitCollider in hitColliders)
                {
                    interactable = hitCollider.gameObject.GetComponent<Interactable>();
                    if (interactable != null)
                    {
                        interactable.Interact(this);
                    }
                }
                if (InteractTrigger != null)
                {
                    InteractTrigger.Trigger();
                }
                break;
            case Actions.StaffLight:
                if (StaffLightTrigger != null)
                {
                    StaffLightTrigger.Trigger();
                }
                break;
        }
    }

    public void ProcessEvent(Events evento)
    {
        switch (evento)
        {
            case Events.DisableMovement:
                _movementAllowed = false;
                break;
            case Events.EnableMovement:
                _movementAllowed = true;
                break;
        }
    }

    private bool isMoving;

    [HideInInspector]
    public Health health;

    void Start()
    {
        _initialScaleX = gameObject.transform.localScale.x;
        isMoving = false;
        InitComponents();
        InitSingletons();
        if (inventory != null)
        {
            InventoryManager.Instance.getInventory(inventory);
        }
        GenericEventManager.Instance.TriggerStringEvent(gameObject, "DataLoaded", "",0);
    }

    protected void InitComponents()
    {
        if (_body == null)
        {
            _body = GetComponent<Rigidbody2D>();
        }
        if (animator == null)
        {
            animator = GetComponent<Animator>();
        }
        if (health == null)
        {
            health = GetComponent<Health>();
        }
    }

    protected void InitSingletons()
    {
        _ = InventoryManager.Instance;
        _ = I18nManager.Instance;
        TimeManager timeManager = TimeManager.Instance;
        PersistanceManager.Instance.LoadData();
        timeManager.Initialize();
        GenericEventManager.Instance.TriggerStringEvent(gameObject, "SpookChanged", "",0);
    }

    void FixedUpdate()
    {
        if (Input.GetAxisRaw("Interact")!=0&& _interactTimer>=InteractDelay)
        {
            _interactTimer = 0;
            PerformAction(Actions.Interact);
        }
        else if(_interactTimer < InteractDelay)
        {
            _interactTimer += Time.deltaTime;
        }
        if (Input.GetAxisRaw("Fire2") != 0 && _staffLightTimer >= StaffLightDelay)
        {
            _staffLightTimer = 0;
            PerformAction(Actions.StaffLight);
        }
        else if (_staffLightTimer < StaffLightDelay)
        {
            _staffLightTimer += Time.deltaTime;
        }
        if (_movementAllowed)
        {
            float xInput = 0;
            float yInput = 0;
            if (joystick != null)
            {
                xInput = joystick.Horizontal;
                yInput = joystick.Vertical;
            }
            xInput += Input.GetAxis("Horizontal");
            yInput += Input.GetAxis("Vertical");
            if (xInput < 0)
            {
                gameObject.transform.localScale = new Vector3(
                    -_initialScaleX,
                    gameObject.transform.localScale.y,
                    gameObject.transform.localScale.z
                );
            }
            else if (xInput > 0)
            {
                gameObject.transform.localScale = new Vector3(
                    _initialScaleX,
                    gameObject.transform.localScale.y,
                    gameObject.transform.localScale.z
                );
            }
            isMoving = (xInput != 0 || yInput != 0);
            if (isMoving)
            {
                var moveVector = new Vector3(xInput, yInput, 0);
                _body.MovePosition(new Vector2(
                    (transform.position.x + moveVector.x * speed * Time.deltaTime),
                    transform.position.y + moveVector.y * speed * Time.deltaTime)
                );
                if (animator != null)
                {
                    animator.SetFloat("xInput", xInput);
                    animator.SetFloat("yInput", yInput);
                }
            }
        }
        else
        {
            isMoving = false;
        }
        
        if (animator != null)
        {
            animator.SetBool("isWalking", isMoving);
        }
    }

    public void ClearData()
    {
        if (health != null)
        {
            health.ClearData();
        }
        _spookAmount = 0;
    }

    public void LoadData(PlayerData playerData)
    {
        if (health != null)
        {
            health.LoadData(playerData.healthData);
        }
        AddSpook(playerData.SpookAmount);
        InitSpookFeedback();
    }

    public void SaveData(PlayerData playerData)
    {
        InitComponents();
        health.SaveData(playerData.healthData);
        playerData.SpookAmount = _spookAmount;
    }
}
