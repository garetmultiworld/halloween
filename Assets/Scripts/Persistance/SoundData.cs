﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SoundData
{
    public float masterVolume=100;
    public float musicVolume=100;
    public float sfxVolume=100;
}
