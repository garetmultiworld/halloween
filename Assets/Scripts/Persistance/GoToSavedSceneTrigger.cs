﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToSavedSceneTrigger : TriggerInterface
{
    public override void Cancel()
    {
        
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        PersistanceManager.Instance.LoadData(true);
    }
}
