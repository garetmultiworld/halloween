﻿public class TutorialPlayedTrigger : TriggerInterface
{
    public override void Cancel()
    {

    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        PersistanceManager.Instance.TutorialPlayed = true;
    }
}
