﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveDataTrigger : TriggerInterface
{
    public override void Cancel()
    {
        
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        PersistanceManager.Instance.SaveData();
    }
}
