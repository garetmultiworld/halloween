﻿using UnityEngine;

public class IfTutorialPlayedTrigger : TriggerInterface
{
    public TriggerInterface IfNotPlayed;
    public TriggerInterface IfPlayed;

    public override void Cancel()
    {

    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (PersistanceManager.Instance.TutorialPlayed)
        {
            if (IfPlayed != null)
            {
                IfPlayed.Trigger();
            }
        }
        else
        {
            if (IfNotPlayed != null)
            {
                IfNotPlayed.Trigger();
            }
        }
    }
}
