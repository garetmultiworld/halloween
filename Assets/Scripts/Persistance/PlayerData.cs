﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{

    public I18nLanguages CurrentLanguage;
    public HealthData healthData=new HealthData();
    public float SpookAmount = 0;
    public InventoryData[] inventories;
    public float CurrentTime = 0;
    public string CurrentScene = "Town";
    public bool CinematicPlayed = false;
    public bool TutorialPlayed = false;
    public int GraphicsLevel = 2;
    public SoundData soundData = new SoundData();

    [HideInInspector]
    public string hash;

    public string GetJson(bool prettyPrint=false)
    {
        return JsonUtility.ToJson(this, prettyPrint);
    }

    public void SetJson(string json)
    {
        JsonUtility.FromJsonOverwrite(json, this);
    }

}
