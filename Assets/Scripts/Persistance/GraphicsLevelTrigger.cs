﻿public class GraphicsLevelTrigger : TriggerInterface
{

    public int GraphicsLevel=2;

    public override void Cancel()
    {

    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        PersistanceManager.Instance.GraphicsLevel = GraphicsLevel;
    }
}
