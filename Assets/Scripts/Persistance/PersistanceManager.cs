﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PersistanceManager : MonoBehaviour
{
    #region Static Instance
    private static PersistanceManager instance;
    public static PersistanceManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<PersistanceManager>();
                if (instance == null)
                {
                    instance = new GameObject("Spawned I18nManager", typeof(PersistanceManager)).GetComponent<PersistanceManager>();
                }
            }
            instance.Initialize();
            return instance;
        }
        private set
        {
            instance = value;
        }
    }
    #endregion

    public enum StorageMethod
    {
        PrettyPrint,
        PlainText,
        Hashed,
        Encrypted
    }

    [HideInInspector]
    public StorageMethod storageMethod=StorageMethod.PrettyPrint;
    public string[] RecordableScenes = { 
        "House",
        "Town"
    };
    public bool CinematicPlayed = false;
    public bool TutorialPlayed = false;
    public int GraphicsLevel = 2;

    protected PlayerData playerData=new PlayerData();

    private string _fileName;
    private string _tempFileName;
    private readonly byte[] modulus = { 127, 14, 20, 77, 199, 173, 225, 112, 255, 222, 250, 45, 71, 195, 194, 234, 6, 27, 236, 239, 171, 131, 190, 83, 235, 96, 164, 64, 124, 135, 213, 52, 83, 69, 47, 162, 50, 240, 157, 185, 96, 212, 186, 135, 138, 72, 89, 188, 50, 0, 86, 12, 154, 104, 90, 238, 102, 227, 216, 25, 66, 38, 188, 248, 130, 188, 236, 70, 159, 231, 99, 59, 95, 14, 186, 30, 26, 141, 52, 179, 30, 193, 176, 213, 1, 89, 173, 7, 107, 219, 140, 99, 177, 144, 73, 252, 55, 193, 192, 216, 74, 107, 253, 118, 14, 137, 133, 183, 175, 50, 213, 118, 64, 98, 11, 204, 71, 26, 88, 188, 42, 203, 160, 12, 249, 236, 242, 87, 166, 174, 139, 109, 155, 201, 158, 171, 12, 184, 51, 143, 200, 216, 97, 107, 195, 159, 197, 160, 129, 4, 167, 62, 223, 113, 171, 59, 5, 151, 162, 97, 86, 117, 97, 18, 229, 9, 149, 39, 110, 254, 164, 80, 223, 147, 229, 36, 113, 34, 39, 204, 13, 147, 33, 220, 14, 10, 8, 182, 228, 251, 165, 77, 147, 20, 245, 70, 80, 132, 98, 200, 56, 218, 230, 38, 173, 8, 199, 217, 71, 156, 187, 118, 50, 149, 193, 245, 183, 160, 202, 169, 92, 217, 235, 10, 47, 30, 236, 7, 176, 52, 194, 67, 231, 247, 139, 4, 194, 211, 79, 169, 157, 207, 181, 11, 25, 67, 108, 190, 195, 246, 1, 229, 83, 159, 116, 156 };
    private readonly byte[] exponent = { 1, 1, 0 };
    private readonly byte[] salt = { 222, 129, 75, 158, 138, 88, 150, 84, 72, 32, 125, 182, 147, 212, 199, 193 };
    private readonly RSA rsa = RSA.Create();
    private RSAParameters rsaKeyInfo = new RSAParameters();

    PersistanceManager()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    void Initialize()
    {
        _fileName = System.IO.Path.Combine(Application.persistentDataPath, "save.data");
        _tempFileName = System.IO.Path.Combine(Application.persistentDataPath, "tmp.tmp");
        rsaKeyInfo.Modulus = modulus;
        rsaKeyInfo.Exponent = exponent;
        rsa.ImportParameters(rsaKeyInfo);
    }

    public void SaveData()
    {
        //rotate the name file, so that it one fails, you can still go back to the previous version
        //save on checkpoints
        //also, throttle the saving process
        Debug.Log("Saving into: "+ _fileName);
        RecopilateData();
        float prev = Time.time;
        switch (storageMethod)
        {
            case StorageMethod.Encrypted:
                SaveEncrypted();
                break;
            case StorageMethod.Hashed:
                SaveHashed();
                break;
            case StorageMethod.PlainText:
                SavePlainText();
                break;
            case StorageMethod.PrettyPrint:
                SavePrettyPrint();
                break;
        }
        Debug.Log("Done Saving, total time: "+(Time.time-prev));
    }

    protected void SaveEncrypted()
    {
        SetHash(playerData);
        File.WriteAllText(_tempFileName, playerData.GetJson());
        FileEncrypt(_tempFileName,_fileName, Encoding.UTF8.GetString(modulus));
        File.Delete(_tempFileName);
    }

    protected void SaveHashed()
    {
        SetHash(playerData);
        File.WriteAllText(_fileName, playerData.GetJson());
    }

    protected void SavePlainText()
    {
        File.WriteAllText(_fileName, playerData.GetJson());
    }

    protected void SavePrettyPrint()
    {
        File.WriteAllText(_fileName, playerData.GetJson(true));
    }

    protected void SetHash(PlayerData data)
    {
        SHA256Managed crypt = new SHA256Managed();
        data.hash = string.Empty;
        string saveStateString = data.GetJson();
        string hash = string.Empty;
        byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(saveStateString), 0, Encoding.UTF8.GetByteCount(saveStateString));
        foreach (byte bit in crypto)
        {
            hash += bit.ToString("x2");
        }
        foreach (byte bit in salt)
        {
            hash += bit.ToString("x2");
        }
        data.hash = hash;
    }

    public void LoadData(bool switchScene = false)
    {
        if (File.Exists(_fileName))
        {
            Debug.Log("Loading from: " + _fileName);
            float prev = Time.time;
            switch (storageMethod)
            {
                case StorageMethod.Encrypted:
                    LoadEncrypted();
                    break;
                case StorageMethod.Hashed:
                    LoadHashed();
                    break;
                case StorageMethod.PlainText:
                    LoadPlainText();
                    break;
                case StorageMethod.PrettyPrint:
                    LoadPrettyPrint();
                    break;
            }
            InitLoadedData(switchScene);
            Debug.Log("Done Loading, total time: " + (Time.time - prev));
        }
        else
        {
            Debug.Log("Save file doesn't exist yet: "+_fileName);
        }
        if (switchScene)
        {
            SceneManager.LoadScene(playerData.CurrentScene);
        }
    }

    protected void LoadEncrypted()
    {
        FileDecrypt(_fileName, _tempFileName, Encoding.UTF8.GetString(modulus));
        PlayerData tempData = new PlayerData();
        string jsonData = File.ReadAllText(_tempFileName);
        File.Delete(_tempFileName);
        tempData.SetJson(jsonData);
        string tmpHash = tempData.hash;
        SetHash(tempData);
        if (tmpHash.Equals(tempData.hash))
        {
            playerData.SetJson(jsonData);
        }
        else
        {
            Debug.LogError("Data has been corrupted");
        }
    }

    protected void LoadHashed()
    {
        PlayerData tempData = new PlayerData();
        string jsonData = File.ReadAllText(_fileName);
        tempData.SetJson(jsonData);
        string tmpHash = tempData.hash;
        SetHash(tempData);
        if (tmpHash.Equals(tempData.hash))
        {
            playerData.SetJson(jsonData);
        }
        else
        {
            Debug.LogError("Data has been corrupted");
        }
    }

    protected void LoadPlainText()
    {
        playerData.SetJson(File.ReadAllText(_fileName));
    }

    protected void LoadPrettyPrint()
    {
        playerData.SetJson(File.ReadAllText(_fileName));
    }

    protected void InitLoadedData(bool switchScene=false)
    {
        I18nManager.Instance.CurrentLanguage = playerData.CurrentLanguage;
        if(BasicPlayerController.instance != null){
            BasicPlayerController.instance.LoadData(playerData);
        }
        InventoryManager.Instance.LoadData(playerData.inventories);
        TimeManager.Instance.LoadData(playerData);
        CinematicPlayed = playerData.CinematicPlayed;
        TutorialPlayed = playerData.TutorialPlayed;
        GraphicsLevel = playerData.GraphicsLevel;
        InitSoundData();
    }

    private void ClearSoundData()
    {
        AkSoundEngine.SetRTPCValue("MasterVolume", 100);
        AkSoundEngine.SetRTPCValue("MusicVolume", 100);
        AkSoundEngine.SetRTPCValue("SfxVolume", 100);
    }

    private void InitSoundData()
    {
        AkSoundEngine.SetRTPCValue("MasterVolume", playerData.soundData.masterVolume);
        AkSoundEngine.SetRTPCValue("MusicVolume", playerData.soundData.musicVolume);
        AkSoundEngine.SetRTPCValue("SfxVolume", playerData.soundData.sfxVolume);
    }

    protected void RecopilateData()
    {
        playerData.CurrentLanguage = I18nManager.Instance.CurrentLanguage;
        if (BasicPlayerController.instance != null)
        {
            BasicPlayerController.instance.SaveData(playerData);
        }
        playerData.inventories = new InventoryData[InventoryManager.Instance.Inventories.Count];
        InventoryManager.Instance.SaveData(playerData.inventories);
        playerData.CurrentTime = TimeManager.Instance.GetCurrentTime();
        string scene = SceneManager.GetActiveScene().name;
        if (RecordableScenes.Contains(scene))
        {
            playerData.CurrentScene=scene;
        }
        playerData.CinematicPlayed = CinematicPlayed;
        playerData.TutorialPlayed = TutorialPlayed;
        playerData.GraphicsLevel = GraphicsLevel;
    }

    public static byte[] GenerateRandomSalt()
    {
        byte[] data = new byte[32];

        using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
        {
            for (int i = 0; i < 10; i++)
            {
                // Fille the buffer with the generated data
                rng.GetBytes(data);
            }
        }

        return data;
    }

    private void FileEncrypt(string inputFile, string outputFile, string password)
    {
        //http://stackoverflow.com/questions/27645527/aes-encryption-on-large-files

        //generate random salt
        byte[] salt = GenerateRandomSalt();

        //create output file name
        FileStream fsCrypt = new FileStream(outputFile, FileMode.Create);

        //convert password string to byte arrray
        byte[] passwordBytes = System.Text.Encoding.UTF8.GetBytes(password);

        //Set Rijndael symmetric encryption algorithm
        RijndaelManaged AES = new RijndaelManaged();
        AES.KeySize = 256;
        AES.BlockSize = 128;
        AES.Padding = PaddingMode.PKCS7;

        //http://stackoverflow.com/questions/2659214/why-do-i-need-to-use-the-rfc2898derivebytes-class-in-net-instead-of-directly
        //"What it does is repeatedly hash the user password along with the salt." High iteration counts.
        var key = new Rfc2898DeriveBytes(passwordBytes, salt, 50000);
        AES.Key = key.GetBytes(AES.KeySize / 8);
        AES.IV = key.GetBytes(AES.BlockSize / 8);

        //Cipher modes: http://security.stackexchange.com/questions/52665/which-is-the-best-cipher-mode-and-padding-mode-for-aes-encryption
        AES.Mode = CipherMode.CFB;

        // write salt to the begining of the output file, so in this case can be random every time
        fsCrypt.Write(salt, 0, salt.Length);

        CryptoStream cs = new CryptoStream(fsCrypt, AES.CreateEncryptor(), CryptoStreamMode.Write);

        FileStream fsIn = new FileStream(inputFile, FileMode.Open);

        //create a buffer (1mb) so only this amount will allocate in the memory and not the whole file
        byte[] buffer = new byte[1048576];
        int read;

        try
        {
            while ((read = fsIn.Read(buffer, 0, buffer.Length)) > 0)
            {
                //throttle here
                cs.Write(buffer, 0, read);
            }

            // Close up
            fsIn.Close();
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.Message);
        }
        finally
        {
            cs.Close();
            fsCrypt.Close();
        }
    }

    private void FileDecrypt(string inputFile, string outputFile, string password)
    {
        byte[] passwordBytes = System.Text.Encoding.UTF8.GetBytes(password);
        byte[] salt = new byte[32];

        FileStream fsCrypt = new FileStream(inputFile, FileMode.Open);
        fsCrypt.Read(salt, 0, salt.Length);

        RijndaelManaged AES = new RijndaelManaged();
        AES.KeySize = 256;
        AES.BlockSize = 128;
        var key = new Rfc2898DeriveBytes(passwordBytes, salt, 50000);
        AES.Key = key.GetBytes(AES.KeySize / 8);
        AES.IV = key.GetBytes(AES.BlockSize / 8);
        AES.Padding = PaddingMode.PKCS7;
        AES.Mode = CipherMode.CFB;

        CryptoStream cs = new CryptoStream(fsCrypt, AES.CreateDecryptor(), CryptoStreamMode.Read);

        FileStream fsOut = new FileStream(outputFile, FileMode.Create);

        int read;
        byte[] buffer = new byte[1048576];

        try
        {
            while ((read = cs.Read(buffer, 0, buffer.Length)) > 0)
            {
                //throttle here
                fsOut.Write(buffer, 0, read);
            }
        }
        catch (CryptographicException ex_CryptographicException)
        {
            Console.WriteLine("CryptographicException error: " + ex_CryptographicException.Message);
        }
        catch (Exception ex)
        {
            Console.WriteLine("Error: " + ex.Message);
        }

        try
        {
            cs.Close();
        }
        catch (Exception ex)
        {
            Console.WriteLine("Error by closing CryptoStream: " + ex.Message);
        }
        finally
        {
            fsOut.Close();
            fsCrypt.Close();
        }
    }

    public void ClearData()
    {
        if (File.Exists(_fileName))
        {
            File.Delete(_fileName);
        }
        playerData = new PlayerData();
        if (BasicPlayerController.instance != null)
        {
            BasicPlayerController.instance.ClearData();
        }
        InventoryManager.Instance.ClearData();
        TimeManager.Instance.ClearData();
        CinematicPlayed = false;
        TutorialPlayed = false;
        GraphicsLevel = 2;
        ClearSoundData();
    }

    public void SetMasterVolume(float volume)
    {
        playerData.soundData.masterVolume = volume;
    }

    public void SetMusicVolume(float volume)
    {
        playerData.soundData.musicVolume = volume;
    }

    public void SetSfxVolume(float volume)
    {
        playerData.soundData.sfxVolume = volume;
    }

    public SoundData GetSoundData()
    {
        return playerData.soundData;
    }

}
