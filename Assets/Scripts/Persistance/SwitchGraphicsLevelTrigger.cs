﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchGraphicsLevelTrigger : TriggerInterface
{
    public TriggerInterface Low;
    public TriggerInterface Medium;
    public TriggerInterface High;

    public override void Cancel()
    {

    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        switch (PersistanceManager.Instance.GraphicsLevel)
        {
            case 0:
                if (Low != null)
                {
                        Low.Trigger();
                }
                break;
            case 1:
                if (Medium != null)
                {
                    Medium.Trigger();
                }
                break;
            case 2:
                if (High != null)
                {
                    High.Trigger();
                }
                break;
        }
    }
}
