﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HealthData
{

    public float MaxHealth=100;
    public float InitialHealth=100;
    public float CurrentHealth=100;

}
