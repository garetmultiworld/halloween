﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IfCinematicPlayedTrigger : TriggerInterface
{

    public TriggerInterface IfNotPlayed;
    public TriggerInterface IfPlayed;

    public override void Cancel()
    {

    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (PersistanceManager.Instance.CinematicPlayed)
        {
            if (IfPlayed != null)
            {
                IfPlayed.Trigger();
            }
        }
        else
        {
            if (IfNotPlayed != null)
            {
                IfNotPlayed.Trigger();
            }
        }
    }
}
