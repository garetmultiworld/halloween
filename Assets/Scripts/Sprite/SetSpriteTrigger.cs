﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetSpriteTrigger : TriggerInterface
{

    public SpriteRenderer spriteRenderer;
    public Sprite sprite;

    public override void Cancel()
    {

    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        spriteRenderer.sprite = sprite;
    }
}
