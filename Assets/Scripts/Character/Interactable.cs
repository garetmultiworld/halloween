﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{

    public TriggerInterface Trigger;

    public void Interact(BasicPlayerController basicPlayerController)
    {
        if (Trigger != null)
        {
            Trigger.Trigger();
        }
    }
}
