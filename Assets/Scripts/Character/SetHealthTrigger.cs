﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetHealthTrigger : TriggerInterface
{
    public float Amount;

    public override void Cancel()
    {
        
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        BasicPlayerController.instance.health.SetHealth(Amount);
    }
}
