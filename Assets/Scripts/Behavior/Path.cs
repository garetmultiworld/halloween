﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Path
{

#if UNITY_EDITOR
	[HideInInspector]
	public bool showLocations= false;
#endif

	public CycleOptions CycleOption = CycleOptions.Loop;
	public MovementDirection movementDirection = MovementDirection.Ascending;
	public float MinDistanceToGoal = .1f;
	public PathLocation[] Locations= { };
	public bool GlobalPositioning = false;


	private Vector3 _relativePosition;
	private int _direction = 1;
	private int _previousIndex = -1;
	private PathLocation _previousLocation = null;
	private int _currentIndex = -1;
	private PathLocation _currentLocation = null;
	protected bool _endReached = false;

	public enum CycleOptions
	{
		BackAndForth,
		Loop,
		OnlyOnce,
		StopAtBounds
	}

	public enum MovementDirection
	{
		Ascending,
		Descending
	}

	public void Initialize(Vector3 initialPos)
    {
		_endReached = false;
		if (movementDirection == MovementDirection.Ascending)
		{
			_direction = 1;
		}
		else
		{
			_direction = -1;
		}
		_relativePosition = initialPos;
		GoToNextLocation();
	}

    internal bool EndReached()
    {
		return _endReached;
    }

    public void GoToNextLocation()
	{
		_previousIndex = _currentIndex;
		_previousLocation = _currentLocation;
        switch (CycleOption)
        {
			case CycleOptions.Loop:
				_currentIndex += _direction;
				if (_currentIndex < 0)
				{
					_currentIndex = Locations.Length - 1;
				}
				else if (_currentIndex > Locations.Length - 1)
				{
					_currentIndex = 0;
				}
				break;
			case CycleOptions.BackAndForth:
				if (_currentIndex <= 0)
				{
					_direction = 1;
				}
				else if (_currentIndex >= Locations.Length - 1)
				{
					_direction = -1;
				}
				_currentIndex += _direction;
				break;
			case CycleOptions.OnlyOnce:
				if (_currentIndex <= 0)
				{
					_direction = 1;
				}
				else if (_currentIndex >= Locations.Length - 1)
				{
					_direction = 0;
					_endReached = true;
				}
				_currentIndex += _direction;
				break;
			case CycleOptions.StopAtBounds:
				if (_currentIndex <= 0)
				{
					if (_direction == -1)
					{
						_endReached = true;
					}
					_direction = 1;
				}
				else if (_currentIndex >= Locations.Length - 1)
				{
					if (_direction == 1)
					{
						_endReached = true;
					}
					_direction = -1;
				}
				_currentIndex += _direction;
				break;
		}
		_currentLocation = Locations[_currentIndex];
	}

	public int GetCurrentIndex()
    {
		return _currentIndex;
    }

	public PathLocation GetCurrentLocation()
    {
		return _currentLocation;
    }

	public bool Arrived(Vector3 position)
    {
		Vector3 currentPos = GetCurrentPosition();
		float _distanceToNextPoint = (position - currentPos).magnitude;
		return _distanceToNextPoint < MinDistanceToGoal;
	}

    public Vector3 GetCurrentPosition()
    {
		if (GlobalPositioning)
        {
			return _currentLocation.Location;
		}
        else
        {
			return _currentLocation.Location + _relativePosition;
		}
	}

}
