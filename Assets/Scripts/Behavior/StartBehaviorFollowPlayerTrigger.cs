﻿public class StartBehaviorFollowPlayerTrigger : TriggerInterface
{

    public BehaviorFollowPlayer behavior;

    public override void Cancel()
    {

    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        behavior.StartFollowing();
    }
}
