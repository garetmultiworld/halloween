﻿using UnityEditor;

[CustomEditor(typeof(BehaviorFollowPlayer), true)]
public class BehaviorFollowPlayerEditor : Editor
{

    protected bool _hadChanges;

    protected void SetUpPrefabConflict(BehaviorFollowPlayer behavior)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(behavior, "BehaviorFollowPlayer");
    }

    protected void StorePrefabConflict(BehaviorFollowPlayer behavior)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(behavior);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(behavior.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        _hadChanges = false;
        BehaviorFollowPlayer behaviorFollowPlayer = (BehaviorFollowPlayer)target;
        SetUpPrefabConflict(behaviorFollowPlayer);
        TriggerInterface prevTrigger = behaviorFollowPlayer.OnStartFollow;
        behaviorFollowPlayer.OnStartFollow = EditorUtils.SelectTrigger("On Start Follow", behaviorFollowPlayer.OnStartFollow);
        if (prevTrigger != behaviorFollowPlayer.OnStartFollow)
        {
            _hadChanges = true;
        }
        prevTrigger = behaviorFollowPlayer.OnStopFollow;
        behaviorFollowPlayer.OnStopFollow = EditorUtils.SelectTrigger("On Stop Follow", behaviorFollowPlayer.OnStopFollow);
        if (prevTrigger != behaviorFollowPlayer.OnStopFollow)
        {
            _hadChanges = true;
        }
        float prevFloat = behaviorFollowPlayer.MovementSpeed;
        behaviorFollowPlayer.MovementSpeed = EditorGUILayout.FloatField("Movement Speed", behaviorFollowPlayer.MovementSpeed);
        if (prevFloat != behaviorFollowPlayer.MovementSpeed)
        {
            _hadChanges = true;
        }
        bool prevBool = behaviorFollowPlayer.StopOnArrive;
        behaviorFollowPlayer.StopOnArrive = EditorUtils.Checkbox("Stop On Arrive", behaviorFollowPlayer.StopOnArrive);
        if (prevBool != behaviorFollowPlayer.StopOnArrive)
        {
            _hadChanges = true;
        }
        if (behaviorFollowPlayer.StopOnArrive)
        {
            prevFloat = behaviorFollowPlayer.MinDistanceToGoal;
            behaviorFollowPlayer.MinDistanceToGoal = EditorGUILayout.FloatField("Min Distance To Goal", behaviorFollowPlayer.MinDistanceToGoal);
            if (prevFloat != behaviorFollowPlayer.MinDistanceToGoal)
            {
                _hadChanges = true;
            }
            prevTrigger = behaviorFollowPlayer.OnArrive;
            behaviorFollowPlayer.OnArrive = EditorUtils.SelectTrigger("On Arrive", behaviorFollowPlayer.OnArrive);
            if (prevTrigger != behaviorFollowPlayer.OnArrive)
            {
                _hadChanges = true;
            }
        }
        StorePrefabConflict(behaviorFollowPlayer);
    }
}
