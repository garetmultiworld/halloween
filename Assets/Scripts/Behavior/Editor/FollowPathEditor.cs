﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(FollowPath))]
public class FollowPathEditor : Editor
{
    protected bool _hadChanges = false;
    protected FollowPath followPath;

    protected void SetUpPrefabConflict(FollowPath path)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(path, "FollowPath");
    }

    protected void StorePrefabConflict(FollowPath path)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(path);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(path.gameObject.scene);
    }

    public void OnSceneGUI()
    {
        followPath = (FollowPath)target;
        PathLocation location;
        Rigidbody2D body = followPath.GetComponent<Rigidbody2D>();
        if (body == null)
        {
            EditorGUILayout.HelpBox("This object must have a Rigidbody2D", MessageType.Warning);
            if (GUILayout.Button("Add Rigidbody2D"))
            {
                _ = followPath.gameObject.AddComponent(typeof(Rigidbody2D)) as Rigidbody2D;
                _hadChanges = true;
            }
        }
        for (int iLocation=0;iLocation<followPath.path.Locations.Length;iLocation++) {
            location = followPath.path.Locations[iLocation];
            Vector3 prevPos = location.Location;
            Handles.Label(location.Location, iLocation.ToString());
            if (followPath.path.GlobalPositioning)
            {
                location.Location = Handles.PositionHandle(location.Location, Quaternion.identity);
            }
            else
            {
                location.Location = Handles.PositionHandle(location.Location+ followPath.transform.position, Quaternion.identity)- followPath.transform.position;
            }
            if (prevPos != location.Location)
            {
                _hadChanges = true;
            }
        }
    }

    public override void OnInspectorGUI()
    {
        followPath = (FollowPath)target;
        SetUpPrefabConflict(followPath);
        /**/
        if (EditorApplication.isPlaying)
        {
            GUILayout.Label("InstanceID: " +followPath.GetInstanceID());
        }
        /**/
        FollowPathInspector();
        followPath.showPath = EditorGUILayout.Foldout(
            followPath.showPath,
            "Path"
        );
        if (followPath.showPath)
        {
            PathInspector(followPath.path);
        }
        StorePrefabConflict(followPath);
    }

    protected void FollowPathInspector()
    {
        bool prevBool = followPath.AutoStart;
        followPath.AutoStart = EditorUtils.Checkbox(
            "Auto Start",
            followPath.AutoStart
        );
        if (prevBool != followPath.AutoStart)
        {
            _hadChanges = true;
        }
        prevBool = followPath.OneByOne;
        followPath.OneByOne = EditorUtils.Checkbox(
            "One By One",
            followPath.OneByOne
        );
        if (prevBool != followPath.OneByOne)
        {
            _hadChanges = true;
        }
        float prevFloat = followPath.MovementSpeed;
        followPath.MovementSpeed = EditorGUILayout.FloatField("Movement Speed", followPath.MovementSpeed);
        if (prevFloat != followPath.MovementSpeed)
        {
            _hadChanges = true;
        }
        TriggerInterface prevTrigger = followPath.OnStartMovement;
        followPath.OnStartMovement = EditorUtils.SelectTrigger("On Start Movement", followPath.OnStartMovement);
        if (prevTrigger != followPath.OnStartMovement)
        {
            _hadChanges = true;
        }
        prevTrigger = followPath.OnStopMovement;
        followPath.OnStopMovement = EditorUtils.SelectTrigger("On Stop Movement", followPath.OnStopMovement);
        if (prevTrigger != followPath.OnStopMovement)
        {
            _hadChanges = true;
        }
    }

    protected void PathInspector(Path path)
    {
        Path.CycleOptions prevCycle = path.CycleOption;
        path.CycleOption = (Path.CycleOptions)EditorGUILayout.EnumPopup("Cycle Type",path.CycleOption);
        if (prevCycle != path.CycleOption)
        {
            _hadChanges = true;
        }
        Path.MovementDirection prevMov = path.movementDirection;
        path.movementDirection = (Path.MovementDirection)EditorGUILayout.EnumPopup("Initial Movement",path.movementDirection);
        if (prevMov != path.movementDirection)
        {
            _hadChanges = true;
        }
        float prevFloat = path.MinDistanceToGoal;
        path.MinDistanceToGoal = EditorGUILayout.FloatField("Min Distance To Goal", path.MinDistanceToGoal);
        if (prevFloat != path.MinDistanceToGoal)
        {
            _hadChanges = true;
        }
        bool prevBool = path.GlobalPositioning;
        path.GlobalPositioning = EditorUtils.Checkbox(
            "Global Positions",
            path.GlobalPositioning
        );
        if (prevBool != path.GlobalPositioning)
        {
            _hadChanges = true;
        }
        path.showLocations = EditorGUILayout.Foldout(
            path.showLocations,
            "Locations"
        );
        if (path.showLocations)
        {
            if (path.Locations.Length == 0)
            {
                if (GUILayout.Button(EditorGUIUtility.IconContent("CreateAddNew"), GUILayout.Width(32)))
                {
                    Array.Resize(ref path.Locations, path.Locations.Length + 1);
                    path.Locations[path.Locations.Length - 1] = new PathLocation();
                    _hadChanges = true;
                }
            }
            for (int iLocation = 0; iLocation < path.Locations.Length; iLocation++)
            {
                DrawLocationItem(path, iLocation);
            }
        }
    }

    protected void DrawLocationItem(Path path,int index)
    {
        bool deleted = false;
        GUILayout.BeginHorizontal();
        if (GUILayout.Button(EditorGUIUtility.IconContent("Toolbar Minus"), GUILayout.Width(32)))
        {
            EditorArrayUtils.RemoveAt(ref path.Locations, index);
            deleted = true;
            _hadChanges = true;
        }
        if (GUILayout.Button(new GUIContent("Before", EditorGUIUtility.IconContent("Toolbar Plus").image)))
        {
            EditorArrayUtils.InsertBefore(ref path.Locations, index);
            path.Locations[index] = new PathLocation();
            _hadChanges = true;
        }
        if (GUILayout.Button(new GUIContent("After", EditorGUIUtility.IconContent("Toolbar Plus").image)))
        {
            EditorArrayUtils.InsertAfter(ref path.Locations, index);
            path.Locations[index + 1] = new PathLocation();
            _hadChanges = true;
        }
        if (index == 0)
        {
            GUILayout.Label(" ", GUILayout.Width(32));
        }
        else
        {
            if (GUILayout.Button(EditorGUIUtility.IconContent("scrollup"), GUILayout.Width(32)))
            {
                EditorArrayUtils.MoveUp(ref path.Locations, index);
                _hadChanges = true;
            }
        }
        if (index == (path.Locations.Length - 1))
        {
            GUILayout.Label(" ", GUILayout.Width(32));
        }
        else
        {
            if (GUILayout.Button(EditorGUIUtility.IconContent("scrolldown"), GUILayout.Width(32)))
            {
                EditorArrayUtils.MoveDown(ref path.Locations, index);
                _hadChanges = true;
            }
        }
        GUILayout.EndHorizontal();
        if (!deleted)
        {
            DrawLocationItemDetails(path, index);
        }
    }

    protected void DrawLocationItemDetails(Path path, int index)
    {
        path.Locations[index].showLocation = EditorGUILayout.Foldout(
            path.Locations[index].showLocation,
            index.ToString()
        );
        if (path.Locations[index].showLocation)
        {
            float prevFloat = path.Locations[index].WaitTimeAfterArrival;
            path.Locations[index].WaitTimeAfterArrival = EditorGUILayout.FloatField("Wait Time After Arrival", path.Locations[index].WaitTimeAfterArrival);
            if (prevFloat != path.Locations[index].WaitTimeAfterArrival)
            {
                _hadChanges = true;
            }
            TriggerInterface prevTrigger = path.Locations[index].OnArriveTrigger;
            path.Locations[index].OnArriveTrigger = EditorUtils.SelectTrigger("On Arrive", path.Locations[index].OnArriveTrigger);
            if (prevTrigger != path.Locations[index].OnArriveTrigger)
            {
                _hadChanges = true;
            }
            prevTrigger = path.Locations[index].OnLeaveTrigger;
            path.Locations[index].OnLeaveTrigger = EditorUtils.SelectTrigger("On Leave", path.Locations[index].OnLeaveTrigger);
            if (prevTrigger != path.Locations[index].OnLeaveTrigger)
            {
                _hadChanges = true;
            }



            Vector3 prevPos= path.Locations[index].Location;
            path.Locations[index].Location = EditorGUILayout.Vector3Field("Location:", path.Locations[index].Location);
            if (prevPos != path.Locations[index].Location)
            {
                _hadChanges = true;
            }

        }
    }
}
