﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchLanguageTrigger : TriggerInterface
{

    public I18nLanguages Language;

    public override void Cancel()
    {
        
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        I18nManager.Instance.CurrentLanguage= Language;
    }
}
