﻿using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CustomEditor(typeof(I18nText))]
public class I18nTextEditor : Editor
{

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        I18nText areaTrigger = (I18nText)target;
        areaTrigger.UpdateText();
    }

}
