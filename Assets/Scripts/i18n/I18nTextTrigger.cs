﻿using UnityEngine.UI;

public class I18nTextTrigger : TriggerInterface
{

    public string I18nFolder;
    public string I18nName;
    public Text text;

    public override void Cancel()
    {

    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        text.text = I18nManager.Instance.GetFolderItem(I18nFolder, I18nName);
    }
}
