﻿using System.Collections.Generic;
using UnityEngine;

public class LevelSection : MonoBehaviour
{

    public PlayerStart playerStart;
    public TriggerInterface OnArriveTrigger;
    public TriggerInterface OnLeaveTrigger;
    public List<LevelSectionConnector> levelSectionConnectors = new List<LevelSectionConnector>();


    [HideInInspector]
    public string nextConnectorName="";
    [HideInInspector]
    public LevelSectionConnector nextConnector;
    [HideInInspector]
    public LevelSectionConnector currentConnector;

    public List<LevelSection> GetConnectedSections()
    {
        List<LevelSection> sections=new List<LevelSection>();
        foreach (LevelSectionConnector connector in levelSectionConnectors)
        {
            if (connector != null&& connector.ConnectedTo!=null)
            {
                sections.Add(connector.ConnectedTo.levelSection);
            }
        }
        return sections;
    }

    public string GetNextConnectorName()
    {
        nextConnector = GetDisconnectedConnector();
        if (nextConnector == null)
        {
            return "";
        }
        nextConnectorName = nextConnector.GetNextConnectorName();
        return nextConnectorName;
    }

    public string GetNextConnectorChance()
    {
        nextConnectorName = nextConnector.GetNextConnectorName();
        return nextConnectorName;
    }

    public LevelSectionConnector GetDisconnectedConnector()
    {
        foreach(LevelSectionConnector connector in levelSectionConnectors)
        {
            if (connector!=null&&connector.ConnectedTo==null)
            {
                return connector;
            }
        }
        return null;
    }

    public void SetCurrentConnector(LevelSectionConnector connector)
    {
        currentConnector = connector;
    }

}
