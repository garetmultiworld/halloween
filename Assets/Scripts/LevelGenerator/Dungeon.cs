﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Dungeon", menuName = "Multiworld/Dungeon")]
public class Dungeon : ScriptableObject
{

    public new string name;
    public List<Level> StartingLevel=new List<Level>();
    public List<Level> levels= new List<Level>();
    public int MinLevelAmount;
    public int MaxLevelAmount;

}
