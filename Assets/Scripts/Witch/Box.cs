﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour
{

    public LayerMask FilterLayer;
    public Animator Animator;

    public void OnTriggerEnter2D(Collider2D c)
    {
        if (((1 << c.gameObject.layer) & FilterLayer) != 0)
        {
            Animator.SetTrigger("Touch");
        }
    }

}
