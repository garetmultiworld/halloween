﻿using UnityEngine;

public class StimuliSource : MonoBehaviour
{

    public LayerMask CharacterLayer;
    public string StimuliName;
    public float StimuliRadius;

    public void OnTriggerEnter2D(Collider2D c)
    {
        if (((1 << c.gameObject.layer) & CharacterLayer) != 0)
        {
            Collider2D[] hitColliders = Physics2D.OverlapCircleAll(new Vector2(transform.position.x, transform.position.y), StimuliRadius);
            StimuliSensor Sensor;
            foreach (Collider2D hitCollider in hitColliders)
            {
                Sensor = hitCollider.gameObject.GetComponent<StimuliSensor>();
                if (Sensor != null)
                {
                    Sensor.Stimuli(StimuliName, gameObject);
                }
            }
        }
    }

}
