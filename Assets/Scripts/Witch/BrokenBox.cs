﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrokenBox : MonoBehaviour
{

    public float SoundRadius;

    void OnEnable()
    {
        Collider2D[] hitColliders = Physics2D.OverlapCircleAll(new Vector2(transform.position.x, transform.position.y), SoundRadius);
        StimuliSensor Sensor;
        foreach (Collider2D hitCollider in hitColliders)
        {
            Sensor = hitCollider.gameObject.GetComponent<StimuliSensor>();
            if (Sensor != null)
            {
                Sensor.Stimuli("Sound", gameObject);
            }
        }
    }

}
