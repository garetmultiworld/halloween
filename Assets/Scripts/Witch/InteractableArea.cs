﻿using UnityEngine;

public class InteractableArea : MonoBehaviour
{

    public LayerMask PlayerLayer;
    public GameObject Interactable;

    public void OnTriggerEnter2D(Collider2D c)
    {
        if (((1 << c.gameObject.layer) & PlayerLayer) != 0)
        {
            EnableInteraction();
        }
    }

    public void OnTriggerExit2D(Collider2D c)
    {
        if (((1 << c.gameObject.layer) & PlayerLayer) != 0)
        {
            DisableInteraction();
        }
    }

    private void EnableInteraction()
    {
        GenericEventManager.Instance.TriggerStringEvent(gameObject, "Change Interact Button", "CauldronNear", 0);
        Interactable.SetActive(true);
    }

    private void DisableInteraction()
    {
        GenericEventManager.Instance.TriggerStringEvent(gameObject, "Change Interact Button", "CauldronFar", 0);
        Interactable.SetActive(false);
    }

}
