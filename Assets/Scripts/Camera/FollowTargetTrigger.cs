﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTargetTrigger : TriggerInterface
{

    public CinemachineVirtualCamera cinemachineVirtualCamera;
    public GameObject Target;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        cinemachineVirtualCamera.m_Follow = Target.transform;
    }

}
