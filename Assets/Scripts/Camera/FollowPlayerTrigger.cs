﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayerTrigger : TriggerInterface
{
    public CinemachineVirtualCamera cinemachineVirtualCamera;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        cinemachineVirtualCamera.m_Follow = BasicPlayerController.instance.transform;
    }
}
