﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopFollowingTrigger : TriggerInterface
{

    public CinemachineVirtualCamera cinemachineVirtualCamera;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        cinemachineVirtualCamera.Follow = null;
    }
}
