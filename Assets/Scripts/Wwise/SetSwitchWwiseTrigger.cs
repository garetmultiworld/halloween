﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetSwitchWwiseTrigger : TriggerInterface
{

    public string SwitchGroup;
    public string SwitchState;
    public GameObject Target;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        AkSoundEngine.SetSwitch(SwitchGroup, SwitchState, Target);
    }
}
