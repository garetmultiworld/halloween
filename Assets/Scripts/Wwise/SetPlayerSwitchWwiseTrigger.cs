﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetPlayerSwitchWwiseTrigger : TriggerInterface
{

    public string SwitchGroup;
    public string SwitchState;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        AkSoundEngine.SetSwitch(SwitchGroup, SwitchState, BasicPlayerController.instance.gameObject);
    }
}
