﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IfObjectEnabledTrigger : TriggerInterface
{

    public GameObject TheObject;
    public TriggerInterface IfActive;
    public TriggerInterface IfInactive;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (TheObject != null)
        {
            if (TheObject.activeSelf)
            {
                if (IfActive != null)
                {
                    IfActive.Trigger();
                }
            }
            else
            {
                if (IfInactive != null)
                {
                    IfInactive.Trigger();
                }
            }
        }
    }
    
}
