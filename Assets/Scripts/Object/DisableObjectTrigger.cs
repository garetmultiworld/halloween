﻿using UnityEngine;

public class DisableObjectTrigger : TriggerInterface
{

    public GameObject TheObject;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (TheObject != null)
        {
            TheObject.SetActive(false);
        }
    }
}
