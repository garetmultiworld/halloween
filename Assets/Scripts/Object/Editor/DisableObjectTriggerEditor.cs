﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(DisableObjectTrigger))]
public class DisableObjectTriggerEditor : TriggerInterfaceEditor
{
    protected void SetUpPrefabConflict(DisableObjectTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "DisableObjectTrigger");
    }

    protected void StorePrefabConflict(DisableObjectTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        DisableObjectTrigger disableObjectTrigger = (DisableObjectTrigger)target;
        SetUpPrefabConflict(disableObjectTrigger);
        GameObject prevObject = disableObjectTrigger.TheObject;
        disableObjectTrigger.TheObject = (GameObject)EditorGUILayout.ObjectField(
            "Object",
            disableObjectTrigger.TheObject,
            typeof(GameObject),
            true
        );
        if (prevObject != disableObjectTrigger.TheObject)
        {
            _hadChanges = true;
        }
        StorePrefabConflict(disableObjectTrigger);
    }
}
