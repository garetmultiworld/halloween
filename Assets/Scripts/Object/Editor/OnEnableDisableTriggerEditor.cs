﻿using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CustomEditor(typeof(OnEnableDisableTrigger))]
public class OnEnableDisableTriggerEditor : Editor
{
    protected bool _hadChanges = false;

    protected void SetUpPrefabConflict(OnEnableDisableTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "OnEnableDisableTrigger");
    }

    protected void StorePrefabConflict(OnEnableDisableTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        OnEnableDisableTrigger onEnableDisableTrigger = (OnEnableDisableTrigger)target;
        SetUpPrefabConflict(onEnableDisableTrigger);
        
        TriggerInterface prevTrigger = onEnableDisableTrigger.OnEnableTrigger;
        onEnableDisableTrigger.OnEnableTrigger = EditorUtils.SelectTrigger("On Enable", onEnableDisableTrigger.OnEnableTrigger);
        if (prevTrigger != onEnableDisableTrigger.OnEnableTrigger)
        {
            _hadChanges = true;
        }
        prevTrigger = onEnableDisableTrigger.OnDisableTrigger;
        onEnableDisableTrigger.OnDisableTrigger = EditorUtils.SelectTrigger("On Disable", onEnableDisableTrigger.OnDisableTrigger);
        if (prevTrigger != onEnableDisableTrigger.OnDisableTrigger)
        {
            _hadChanges = true;
        }
        StorePrefabConflict(onEnableDisableTrigger);
    }

}
