﻿using System.Collections;
using UnityEngine;

public class RecoilTrigger : TriggerInterface
{

    public float MinDistance= 1;
    public float MaxDistance = 2;

    public float MinTime = 1;
    public float MaxTime = 2;

    public AnimationCurve AnimCurve;
    public TriggerInterface TriggerOnArrive;

    private bool IsAnimating = false;
    private Vector3 StartingPos;
    private Vector3 TargetPos;
    private float TotalTime;
    private float CurrentTimer = 0;
    private Rigidbody2D body;

    public void Awake()
    {
        body = GetComponent<Rigidbody2D>();
    }

    public override void Cancel()
    {
        IsAnimating = false;
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        //transform.forward if it's on 3D (probably)
        TargetPos = transform.position + (-transform.up * Random.Range(MinDistance, MaxDistance));
        TargetPos.z = 0;
        StartingPos = transform.position;
        TotalTime = Random.Range(MinTime, MaxTime);
        CurrentTimer = 0;
        IsAnimating = true;
        StartCoroutine(Recoil());
    }

    private IEnumerator Recoil()
    {
        body.bodyType = RigidbodyType2D.Kinematic;
        while (IsAnimating)
        {
            float t = CurrentTimer / TotalTime;
            Vector3 newPos = new Vector3(
                StartingPos.x + ((TargetPos.x - StartingPos.x) * AnimCurve.Evaluate(t)),
                StartingPos.y + ((TargetPos.y - StartingPos.y) * AnimCurve.Evaluate(t)),
                transform.position.z
            );
            CurrentTimer += Time.deltaTime;
            if (CurrentTimer >= TotalTime)
            {
                newPos.x = TargetPos.x;
                newPos.y = TargetPos.y;
                IsAnimating = false;
                if (TriggerOnArrive != null)
                {
                    TriggerOnArrive.Trigger();
                }
            }
            transform.position = newPos;
            yield return null;
        }
        body.bodyType = RigidbodyType2D.Dynamic;
    }

}
