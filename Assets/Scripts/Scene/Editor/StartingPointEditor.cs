﻿using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CustomEditor(typeof(StartingPoint))]
public class StartingPointEditor : Editor
{
    protected bool _hadChanges = false;

    protected void SetUpPrefabConflict(StartingPoint startingPoint)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(startingPoint, "StartingPoint");
    }

    protected void StorePrefabConflict(StartingPoint startingPoint)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(startingPoint);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(startingPoint.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        StartingPoint startingPoint = (StartingPoint)target;
        SetUpPrefabConflict(startingPoint);
        string prevString = startingPoint.Label;
        startingPoint.Label = EditorGUILayout.TextField("Label", startingPoint.Label);
        if (
            (string.IsNullOrEmpty(prevString) && !string.IsNullOrEmpty(startingPoint.Label)) ||
            (!string.IsNullOrEmpty(prevString) && string.IsNullOrEmpty(startingPoint.Label)) ||
            (prevString != null && !prevString.Equals(startingPoint.Label))
        )
        {
            _hadChanges = true;
        }
        TriggerInterface prevTrigger = startingPoint.TriggerOnArrive;
        startingPoint.TriggerOnArrive = EditorUtils.SelectTrigger("On Arrive", startingPoint.TriggerOnArrive);
        if (prevTrigger != startingPoint.TriggerOnArrive)
        {
            _hadChanges = true;
        }
        StorePrefabConflict(startingPoint);
    }

}
