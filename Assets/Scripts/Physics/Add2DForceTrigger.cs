﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Add2DForceTrigger : TriggerInterface
{

    public Rigidbody2D body2D;
    public Vector2 direction;
    public float force;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        body2D.AddForce(direction * force);
    }
}
