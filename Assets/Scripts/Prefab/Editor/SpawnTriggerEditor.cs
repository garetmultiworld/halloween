﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SpawnTrigger), true)]
public class SpawnTriggerEditor : TriggerInterfaceEditor
{

    protected void SetUpPrefabConflict(SpawnTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "SpawnTrigger");
    }

    protected void StorePrefabConflict(SpawnTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        _hadChanges = false;
        SpawnTrigger spawnTrigger = (SpawnTrigger)target;
        SetUpPrefabConflict(spawnTrigger);
        spawnTrigger.showSpawnTrigger = EditorGUILayout.Foldout(
            spawnTrigger.showSpawnTrigger,
            "Spawn"
        );
        GameObject prevGO;
        RectBounds prevRectBounds;
        RectBounds.AnchorType prevAnchorType;
        string prevString;
        bool prevBool;
        Transform previewTransform;
        GameObject preview=null;
        if (spawnTrigger.showSpawnTrigger)
        {
            prevBool = spawnTrigger.showPreview;
            spawnTrigger.showPreview = EditorUtils.Checkbox("Show Preview", spawnTrigger.showPreview);
            if (prevBool != spawnTrigger.showPreview)
            {
                spawnTrigger.prefabChanged = true;
                _hadChanges = true;
            }
            previewTransform = spawnTrigger.gameObject.transform.Find("SpawnTriggerPreview");
            if (previewTransform != null)
            {
                preview = previewTransform.gameObject;
            }
            if (!EditorApplication.isPlaying&&spawnTrigger.showPreview&& preview == null)
            {
                preview = new GameObject("SpawnTriggerPreview");
                preview.transform.parent = spawnTrigger.gameObject.transform;
            }
            else if((EditorApplication.isPlaying ||!spawnTrigger.showPreview)&&preview != null)
            {
                DestroyImmediate(preview);
            }
            if (spawnTrigger.Prefab == null)
            {
                EditorGUILayout.HelpBox("There's no prefab assigned", MessageType.Warning);
            }
            prevGO = spawnTrigger.Prefab;
            spawnTrigger.Prefab = (GameObject)EditorGUILayout.ObjectField(
                "Prefab",
                spawnTrigger.Prefab,
                typeof(GameObject),
                true
            );
            if (prevGO != spawnTrigger.Prefab)
            {
                spawnTrigger.prefabChanged = true;
                _hadChanges = true;
            }
            if (!EditorApplication.isPlaying && spawnTrigger.showPreview && spawnTrigger.prefabChanged)
            {
                spawnTrigger.prefabChanged = false;
                foreach (Transform child in preview.transform)
                {
                    DestroyImmediate(child.gameObject);
                }
                if (spawnTrigger.Prefab != null)
                {
                    GameObject instance = Instantiate(spawnTrigger.Prefab, new Vector3(0, 0, 0), Quaternion.identity);
                    instance.transform.parent = preview.transform;
                    instance.transform.localPosition = Vector3.zero;
                }
            }
            EditorGUILayout.HelpBox("If there's no parent assigned, it will spawn at the root of the scene", MessageType.Info);
            prevGO = spawnTrigger.Parent;
            spawnTrigger.Parent = (GameObject)EditorGUILayout.ObjectField(
                "Parent",
                spawnTrigger.Parent,
                typeof(GameObject),
                true
            );
            if (prevGO != spawnTrigger.Parent)
            {
                _hadChanges = true;
            }
            if (spawnTrigger.Bounds == null)
            {
                EditorGUILayout.HelpBox("It will spawn right where the parent is, if you want to have it spawn randomly in an area you can add a RectBounds", MessageType.Info);
                if (GUILayout.Button("Add RectBounds"))
                {
                    spawnTrigger.Bounds = spawnTrigger.gameObject.AddComponent(typeof(RectBounds)) as RectBounds;
                    _hadChanges = true;
                }
            }
            prevRectBounds = spawnTrigger.Bounds;
            spawnTrigger.Bounds = (RectBounds)EditorGUILayout.ObjectField(
                "Bounds",
                spawnTrigger.Bounds,
                typeof(RectBounds),
                true
            );
            if (prevRectBounds != spawnTrigger.Bounds)
            {
                _hadChanges = true;
            }
            if (spawnTrigger.Bounds != null)
            {
                EditorGUILayout.HelpBox("If an anchor is set, the spawner won't use the full rectangle but instead it will use the line associated with the anchor.", MessageType.Info);
                prevAnchorType = spawnTrigger.Anchor;
                spawnTrigger.Anchor = (RectBounds.AnchorType)EditorGUILayout.EnumPopup("Anchor", spawnTrigger.Anchor);
                if (prevAnchorType != spawnTrigger.Anchor)
                {
                    _hadChanges = true;
                }
            }
            EditorGUILayout.HelpBox("A Spawn Initializator allows you to store the spawned object in its Target Holder and then fire all triggers with the assigned label.", MessageType.Info);
            prevGO = spawnTrigger.SpawnInitializator;
            spawnTrigger.SpawnInitializator = (GameObject)EditorGUILayout.ObjectField(
                "Spawn Initializator",
                spawnTrigger.SpawnInitializator,
                typeof(GameObject),
                true
            );
            if (prevGO != spawnTrigger.SpawnInitializator)
            {
                _hadChanges = true;
            }
            if (spawnTrigger.SpawnInitializator != null)
            {
                if (spawnTrigger.SpawnInitializator.GetComponent<TargetHolder>() == null)
                {
                    EditorGUILayout.HelpBox("The Spawn Initializator doesn't have a Target Holder", MessageType.None);
                    if (GUILayout.Button("Add Target Holder to the Spawn Initializator"))
                    {
                        TargetHolder t=spawnTrigger.SpawnInitializator.AddComponent(typeof(TargetHolder)) as TargetHolder;
                    }
                }
                prevString = spawnTrigger.SpawnInitializatorTriggerLabel;
                spawnTrigger.SpawnInitializatorTriggerLabel = EditorGUILayout.TextField("Spawn Initializator Trigger Label: ", spawnTrigger.SpawnInitializatorTriggerLabel);
                if (
                    (string.IsNullOrEmpty(prevString) && !string.IsNullOrEmpty(spawnTrigger.SpawnInitializatorTriggerLabel)) ||
                    (!string.IsNullOrEmpty(prevString) && string.IsNullOrEmpty(spawnTrigger.SpawnInitializatorTriggerLabel)) ||
                    (prevString != null && !prevString.Equals(spawnTrigger.SpawnInitializatorTriggerLabel))
                )
                {
                    _hadChanges = true;
                }
            }
        }
        StorePrefabConflict(spawnTrigger);
    }

}
