﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StimuliTargetTrigger : TriggerInterface
{

    public string Stimuli;
    public StimuliSensor Sensor; 

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        Sensor.Stimuli(Stimuli,gameObject);
    }

}
