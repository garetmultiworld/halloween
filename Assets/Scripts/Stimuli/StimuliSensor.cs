﻿using UnityEngine;

public class StimuliSensor : MonoBehaviour
{

    public OnStimuliTrigger[] StimuliTriggers=new OnStimuliTrigger[0];

    public void Stimuli(string stimuli,GameObject source)
    {
        if(!enabled || !gameObject.activeSelf)
        {
            return;
        }
        foreach (OnStimuliTrigger trigger in StimuliTriggers)
        {
            if (trigger.Stimuli.Equals(stimuli))
            {
                trigger.Source = source;
                trigger.Trigger();
            }
        }
    }

}
