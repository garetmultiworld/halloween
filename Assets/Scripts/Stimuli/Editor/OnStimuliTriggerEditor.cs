using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(OnStimuliTrigger))]
public class OnStimuliTriggerEditor : TriggerInterfaceEditor
{

    protected void SetUpPrefabConflict(OnStimuliTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "OnStimuliTrigger");
    }

    protected void StorePrefabConflict(OnStimuliTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        _hadChanges = false;
        OnStimuliTrigger onStimuliTrigger = (OnStimuliTrigger)target;
        SetUpPrefabConflict(onStimuliTrigger);
        onStimuliTrigger.showOnStimuliTrigger = EditorGUILayout.Foldout(
            onStimuliTrigger.showOnStimuliTrigger,
            "On Stimuli"
        );
        if (onStimuliTrigger.showOnStimuliTrigger)
        {
            string prevString = onStimuliTrigger.Stimuli;
            onStimuliTrigger.Stimuli = EditorGUILayout.TextField("Stimuli", onStimuliTrigger.Stimuli);
            if (
                (string.IsNullOrEmpty(prevString) && !string.IsNullOrEmpty(onStimuliTrigger.Stimuli)) ||
                (!string.IsNullOrEmpty(prevString) && string.IsNullOrEmpty(onStimuliTrigger.Stimuli)) ||
                (prevString != null && !prevString.Equals(onStimuliTrigger.Stimuli))
            )
            {
                _hadChanges = true;
            }
            TriggerInterface prevTrigger = onStimuliTrigger.TheTrigger;
            onStimuliTrigger.TheTrigger = EditorUtils.SelectTrigger("Trigger", onStimuliTrigger.TheTrigger);
            if (prevTrigger != onStimuliTrigger.TheTrigger)
            {
                _hadChanges = true;
            }
        }
        StorePrefabConflict(onStimuliTrigger);
    }

}