﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnStimuliTrigger : TriggerInterface
{

#if UNITY_EDITOR
    public bool showOnStimuliTrigger = true;
#endif

    public string Stimuli="";
    public TriggerInterface TheTrigger;

    [HideInInspector]
    public GameObject Source;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (TheTrigger != null)
        {
            TheTrigger.Trigger();
        }
    }

    public override string GetDescription()
    {
        string desc = base.GetDescription() + " (" + Stimuli;
        if (TheTrigger!=null) {
            desc += ", "+TheTrigger.GetDescription();
        }
        else
        {
            desc += ", no trigger";
        }
        desc += ")";
        return desc;
    }

}
