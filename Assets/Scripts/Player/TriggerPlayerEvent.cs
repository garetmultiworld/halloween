﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerPlayerEvent : TriggerInterface
{
    public BasicPlayerController.Events Evento;

    public override void Cancel()
    {

    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        BasicPlayerController.instance.ProcessEvent(Evento);
    }
}
