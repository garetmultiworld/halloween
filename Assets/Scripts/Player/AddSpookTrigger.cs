﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddSpookTrigger : TriggerInterface
{

    public float Amount=0;

    public override void Cancel()
    {

    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        Debug.Log("AddSpookTrigger: "+Amount);
        BasicPlayerController.instance.AddSpook(Amount);
    }
}
