﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerPlayerAction : TriggerInterface
{

    public BasicPlayerController.Actions Action;

    public override void Cancel()
    {
        
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        BasicPlayerController.instance.PerformAction(Action);
    }
}
