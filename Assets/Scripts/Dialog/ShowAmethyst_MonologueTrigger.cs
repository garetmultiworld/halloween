﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowAmethyst_MonologueTrigger : TriggerInterface
{

#if UNITY_EDITOR
    public bool showAmethyst_MonologueTrigger = true;
#endif

    public string I18nFolder;
    public string I18nName;
    public TriggerInterface callback;

    public override void Cancel()
    {
        Amethyst_MonologueBox.instance.HideDialog();
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        Amethyst_MonologueBox.instance.ShowDialog(I18nFolder, I18nName,callback);
    }
}
