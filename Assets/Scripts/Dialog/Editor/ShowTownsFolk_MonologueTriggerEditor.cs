﻿using UnityEditor;

[CustomEditor(typeof(ShowTownsFolk_MonologueTrigger), true)]
public class ShowTownsFolk_MonologueTriggerEditor : TriggerInterfaceEditor
{
    protected void SetUpPrefabConflict(ShowTownsFolk_MonologueTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "ShowTownsFolk_MonologueTrigger");
    }

    protected void StorePrefabConflict(ShowTownsFolk_MonologueTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        _hadChanges = false;
        ShowTownsFolk_MonologueTrigger showTownsFolk_MonologueTrigger = (ShowTownsFolk_MonologueTrigger)target;
        SetUpPrefabConflict(showTownsFolk_MonologueTrigger);
        showTownsFolk_MonologueTrigger.showTownsFolk_MonologueTrigger = EditorGUILayout.Foldout(
            showTownsFolk_MonologueTrigger.showTownsFolk_MonologueTrigger,
            "Monologue"
        );
        if (showTownsFolk_MonologueTrigger.showTownsFolk_MonologueTrigger)
        {
            string prevString = showTownsFolk_MonologueTrigger.I18nFolder;
            showTownsFolk_MonologueTrigger.I18nFolder = EditorGUILayout.TextField("I18n Folder", showTownsFolk_MonologueTrigger.I18nFolder);
            if (
                (string.IsNullOrEmpty(prevString) && !string.IsNullOrEmpty(showTownsFolk_MonologueTrigger.I18nFolder)) ||
                (!string.IsNullOrEmpty(prevString) && string.IsNullOrEmpty(showTownsFolk_MonologueTrigger.I18nFolder)) ||
                (prevString != null && !prevString.Equals(showTownsFolk_MonologueTrigger.I18nFolder))
            )
            {
                _hadChanges = true;
            }
            prevString = showTownsFolk_MonologueTrigger.I18nName;
            showTownsFolk_MonologueTrigger.I18nName = EditorGUILayout.TextField("I18n Name", showTownsFolk_MonologueTrigger.I18nName);
            if (
                (string.IsNullOrEmpty(prevString) && !string.IsNullOrEmpty(showTownsFolk_MonologueTrigger.I18nName)) ||
                (!string.IsNullOrEmpty(prevString) && string.IsNullOrEmpty(showTownsFolk_MonologueTrigger.I18nName)) ||
                (prevString != null && !prevString.Equals(showTownsFolk_MonologueTrigger.I18nName))
            )
            {
                _hadChanges = true;
            }
            TriggerInterface prevTrigger = showTownsFolk_MonologueTrigger.callback;
            showTownsFolk_MonologueTrigger.callback = EditorUtils.SelectTrigger("Callback", showTownsFolk_MonologueTrigger.callback);
            if (prevTrigger != showTownsFolk_MonologueTrigger.callback)
            {
                _hadChanges = true;
            }
        }
        StorePrefabConflict(showTownsFolk_MonologueTrigger);
    }
}
