﻿using UnityEditor;

[CustomEditor(typeof(ShowAmethyst_MonologueTrigger), true)]
public class ShowAmethyst_MonologueTriggerEditor : TriggerInterfaceEditor
{
    protected void SetUpPrefabConflict(ShowAmethyst_MonologueTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "ShowAmethyst_MonologueTrigger");
    }

    protected void StorePrefabConflict(ShowAmethyst_MonologueTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        _hadChanges = false;
        ShowAmethyst_MonologueTrigger showAmethyst_MonologueTrigger = (ShowAmethyst_MonologueTrigger)target;
        SetUpPrefabConflict(showAmethyst_MonologueTrigger);
        showAmethyst_MonologueTrigger.showAmethyst_MonologueTrigger = EditorGUILayout.Foldout(
            showAmethyst_MonologueTrigger.showAmethyst_MonologueTrigger,
            "Monologue"
        );
        if (showAmethyst_MonologueTrigger.showAmethyst_MonologueTrigger)
        {
            string prevString = showAmethyst_MonologueTrigger.I18nFolder;
            showAmethyst_MonologueTrigger.I18nFolder = EditorGUILayout.TextField("I18n Folder", showAmethyst_MonologueTrigger.I18nFolder);
            if (
                (string.IsNullOrEmpty(prevString) && !string.IsNullOrEmpty(showAmethyst_MonologueTrigger.I18nFolder)) ||
                (!string.IsNullOrEmpty(prevString) && string.IsNullOrEmpty(showAmethyst_MonologueTrigger.I18nFolder)) ||
                (prevString != null && !prevString.Equals(showAmethyst_MonologueTrigger.I18nFolder))
            )
            {
                _hadChanges = true;
            }
            prevString = showAmethyst_MonologueTrigger.I18nName;
            showAmethyst_MonologueTrigger.I18nName = EditorGUILayout.TextField("I18n Name", showAmethyst_MonologueTrigger.I18nName);
            if (
                (string.IsNullOrEmpty(prevString) && !string.IsNullOrEmpty(showAmethyst_MonologueTrigger.I18nName)) ||
                (!string.IsNullOrEmpty(prevString) && string.IsNullOrEmpty(showAmethyst_MonologueTrigger.I18nName)) ||
                (prevString != null && !prevString.Equals(showAmethyst_MonologueTrigger.I18nName))
            )
            {
                _hadChanges = true;
            }
            TriggerInterface prevTrigger = showAmethyst_MonologueTrigger.callback;
            showAmethyst_MonologueTrigger.callback = EditorUtils.SelectTrigger("Callback", showAmethyst_MonologueTrigger.callback);
            if (prevTrigger != showAmethyst_MonologueTrigger.callback)
            {
                _hadChanges = true;
            }
        }
        StorePrefabConflict(showAmethyst_MonologueTrigger);
    }
}
