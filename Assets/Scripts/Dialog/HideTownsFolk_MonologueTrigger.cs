﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideTownsFolk_MonologueTrigger : TriggerInterface
{
    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        TownsFolk_MonologueBox.instance.HideDialog();
    }
}
