﻿using UnityEngine;
using UnityEngine.UI;

public class TownsFolk_MonologueBox : MonoBehaviour
{
    public static TownsFolk_MonologueBox instance;

    public Text text;

    protected TriggerInterface _callback;

    public TownsFolk_MonologueBox()
    {
        instance = this;
    }

    public void ShowDialog(string I18nFolder, string I18nName, TriggerInterface callback)
    {
        text.text = I18nManager.Instance.GetFolderItem(I18nFolder, I18nName);
        gameObject.SetActive(true);
        _callback = callback;
    }

    public void Next()
    {
        if (_callback != null)
        {
            _callback.Trigger();
        }
    }

    public void HideDialog()
    {
        gameObject.SetActive(false);
    }
}
