﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideAmethyst_MonologueTrigger : TriggerInterface
{
    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        Amethyst_MonologueBox.instance.HideDialog();
    }
}
