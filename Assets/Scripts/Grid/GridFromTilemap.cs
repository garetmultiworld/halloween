﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GridFromTilemap : MonoBehaviour
{
    public Grid grid;

    protected Pathfinding pathfinding;
    protected Vector2Int origin = new Vector2Int(0, 0);
    protected Vector2Int finalPoint = new Vector2Int(int.MinValue, int.MinValue);
    protected Vector2Int size = new Vector2Int(0, 0);

    void Start()
    {
        List<Tilemap> tilemaps = new List<Tilemap>();
        Tilemap tilemap;
        List<TilemapCollider2D> tilemapColliders = new List<TilemapCollider2D>();
        TilemapCollider2D tilemapCollider;
        foreach (Transform child in grid.transform)
        {
            tilemap = child.GetComponent<Tilemap>();
            tilemaps.Add(tilemap);
            tilemap.CompressBounds();
            AnalyzeTilemapSize(tilemap);
            tilemapCollider = child.GetComponent<TilemapCollider2D>();
            tilemapColliders.Add(tilemapCollider);
        }
        gameObject.transform.position = new Vector3(origin.x, origin.y, gameObject.transform.position.z);
        size = finalPoint - origin;
        pathfinding = new Pathfinding((int)size.x, (int)size.y);
        for (int tilei = 0; tilei < tilemaps.Count; tilei++)
        {
            if (tilemapColliders[tilei] != null && tilemaps[tilei] != null)
            {
                AnalyzeTilemapCollisions(tilemaps[tilei]);
            }
        }
    }

    private void AnalyzeTilemapCollisions(Tilemap tilemap)
    {
        Vector2Int offset = new Vector2Int(tilemap.origin.x, tilemap.origin.y) - origin;

        Grid<PathNode> pathGrid = pathfinding.GetGrid();

        BoundsInt bounds = tilemap.cellBounds;
        TileBase[] allTiles = tilemap.GetTilesBlock(bounds);
        for (int x = 0; x < bounds.size.x; x++)
        {
            for (int y = 0; y < bounds.size.y; y++)
            {
                TileBase tile = allTiles[x + y * bounds.size.x];
                if (tile != null)
                {
                    pathGrid.GetGridObject(x + offset.x, y + offset.y).isWalkable = false;
                }
            }
        }
    }

    private void AnalyzeTilemapSize(Tilemap tilemap)
    {
        if (tilemap == null)
        {
            return;
        }
        Vector3 lowerLeft = tilemap.origin;
        Vector3 upperRight = lowerLeft + tilemap.size;
        if (lowerLeft.x < origin.x)
        {
            origin.x = (int)lowerLeft.x;
        }
        if (lowerLeft.y < origin.y)
        {
            origin.y = (int)lowerLeft.y;
        }
        if (upperRight.x > finalPoint.x)
        {
            finalPoint.x = (int)upperRight.x;
        }
        if (upperRight.y > finalPoint.y)
        {
            finalPoint.y = (int)upperRight.y;
        }
    }
}
