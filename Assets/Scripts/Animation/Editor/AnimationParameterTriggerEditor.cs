﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(AnimationParameterTrigger))]
public class AnimationParameterTriggerEditor : TriggerInterfaceEditor
{

    protected void SetUpPrefabConflict(AnimationParameterTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "AnimationParameterTrigger");
    }

    protected void StorePrefabConflict(AnimationParameterTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        _hadChanges = false;
        AnimationParameterTrigger animationParameterTrigger = (AnimationParameterTrigger)target;
        SetUpPrefabConflict(animationParameterTrigger);
        animationParameterTrigger.showAnimationParameterTrigger = EditorGUILayout.Foldout(
            animationParameterTrigger.showAnimationParameterTrigger,
            "Animation Parameter"
        );
        if (animationParameterTrigger.showAnimationParameterTrigger)
        {
            if (animationParameterTrigger.Animator == null)
            {
                EditorGUILayout.HelpBox("There's no animator assigned", MessageType.Warning);
            }
            Animator prevAnimator = animationParameterTrigger.Animator;
            animationParameterTrigger.Animator = (Animator)EditorGUILayout.ObjectField(
                "Animator",
                animationParameterTrigger.Animator,
                typeof(Animator),
                true
            );
            if(prevAnimator!= animationParameterTrigger.Animator)
            {
                _hadChanges = true;
            }
            if (string.IsNullOrEmpty(animationParameterTrigger.ParameterName))
            {
                EditorGUILayout.HelpBox("There's no parameter set up", MessageType.Warning);
            }
            string prevString= animationParameterTrigger.ParameterName;
            animationParameterTrigger.ParameterName = EditorGUILayout.DelayedTextField("Parameter Name", animationParameterTrigger.ParameterName);
            if (
                    (string.IsNullOrEmpty(prevString) && !string.IsNullOrEmpty(animationParameterTrigger.ParameterName)) ||
                    (!string.IsNullOrEmpty(prevString) && string.IsNullOrEmpty(animationParameterTrigger.ParameterName)) ||
                    (prevString != null && !prevString.Equals(animationParameterTrigger.ParameterName))
                )
            {
                _hadChanges = true;
            }
            AnimationParameterTrigger.ParamTypes prevType = animationParameterTrigger.Type;
            animationParameterTrigger.Type = (AnimationParameterTrigger.ParamTypes)EditorGUILayout.EnumPopup(animationParameterTrigger.Type);
            if (prevType != animationParameterTrigger.Type)
            {
                _hadChanges = true;
            }
            switch (animationParameterTrigger.Type)
            {
                case AnimationParameterTrigger.ParamTypes.Bool:
                    bool prevBool = animationParameterTrigger.BoolValue;
                    animationParameterTrigger.BoolValue = EditorGUILayout.Toggle("Valor", animationParameterTrigger.BoolValue);
                    if (prevBool != animationParameterTrigger.BoolValue)
                    {
                        _hadChanges = true;
                    }
                    break;
                case AnimationParameterTrigger.ParamTypes.Float:
                    float prevFloat= animationParameterTrigger.FloatValue;
                    animationParameterTrigger.FloatValue = EditorGUILayout.FloatField("Valor", animationParameterTrigger.FloatValue);
                    if (prevFloat != animationParameterTrigger.FloatValue)
                    {
                        _hadChanges = true;
                    }
                    break;
                case AnimationParameterTrigger.ParamTypes.Integer:
                    int prevInt= animationParameterTrigger.IntegerValue;
                    animationParameterTrigger.IntegerValue = EditorGUILayout.IntField("Valor", animationParameterTrigger.IntegerValue);
                    if (prevInt != animationParameterTrigger.IntegerValue)
                    {
                        _hadChanges = true;
                    }
                    break;
            }
        }
        StorePrefabConflict(animationParameterTrigger);
    }
}
