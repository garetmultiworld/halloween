﻿using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

public class EditorUtils
{

    public static void DrawUILine(Color color, int thickness = 1, int padding = 10)
    {
        Rect r = EditorGUILayout.GetControlRect(GUILayout.Height(padding + thickness));
        r.height = thickness;
        r.y += padding / 2;
        r.x -= 2;
        r.width += 6;
        EditorGUI.DrawRect(r, color);
    }

    public static LayerMask SelectLayerMask(string Label,LayerMask layerMask)
    {
        if (layerMask == 0)
        {
            EditorGUILayout.HelpBox("No layer Selected", MessageType.Warning);
        }
        GUILayout.Label(Label);
        LayerMask tempMask = EditorGUILayout.MaskField(
            InternalEditorUtility.LayerMaskToConcatenatedLayersMask(layerMask),
            InternalEditorUtility.layers
        );
        layerMask = InternalEditorUtility.ConcatenatedLayersMaskToLayerMask(tempMask);
        return layerMask;
    }

    public static bool Checkbox(string Label,bool Value) {
        GUILayout.BeginHorizontal();
        GUILayout.Label(Label);
        Value = EditorGUILayout.Toggle(Value);
        GUILayout.EndHorizontal();
        return Value;
    }

    public static TriggerInterface SelectTrigger(string Label,TriggerInterface trigger)
    {
        if (trigger != null)
        {
            EditorUtils.DrawUILine(Color.grey);
        }
        trigger = (TriggerInterface)EditorGUILayout.ObjectField(
            Label,
            trigger,
            typeof(TriggerInterface),
            true
        );
        if (trigger != null)
        {
            string desc = trigger.GetDescription();
            if (string.IsNullOrEmpty(desc))
            {
                desc = "(trigger has no description)";
            }
            EditorGUILayout.HelpBox(desc, MessageType.None);
            EditorUtils.DrawUILine(Color.grey);
        }
        return trigger;
    }

}
