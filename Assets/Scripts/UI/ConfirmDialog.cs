﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfirmDialog : MonoBehaviour
{

    public string Name;
    public TriggerInterface OnShow;
    public TriggerInterface OnYes;
    public TriggerInterface OnNo;

    [HideInInspector]
    public TriggerInterface OnYesCallback;
    [HideInInspector]
    public TriggerInterface OnNoCallback;

    void Start()
    {
        ConfirmDialogManager.Instance.RegisterDialog(this);
        gameObject.SetActive(false);
    }

    public void Yes()
    {
        if (OnYesCallback != null)
        {
            OnYesCallback.Trigger();
            OnYesCallback = null;
        }
        if (OnYes != null)
        {
            OnYes.Trigger();
        }
        gameObject.SetActive(false);
    }

    public void No()
    {
        if (OnNoCallback != null)
        {
            OnNoCallback.Trigger();
            OnNoCallback = null;
        }
        if (OnNo != null)
        {
            OnNo.Trigger();
        }
        gameObject.SetActive(false);
    }

}
