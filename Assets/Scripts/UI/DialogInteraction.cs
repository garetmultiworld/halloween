﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogInteraction : TriggerInterface
{

    public DialogManager dialogManager;
    public bool Fireable;
    public string I18nFolder;
    public DialogInteractionItem[] Dialogs;
    public TriggerInterface TriggerOnStart;
    public TriggerInterface TriggerOnFinish;

    protected GameObject _dialogInstance;
    protected Dialogo _dialog;
    protected int _index=-1;

    void Start()
    {
        _dialogInstance = Instantiate(dialogManager.DialogPrefab, new Vector3(0, 0, 0), Quaternion.identity);
        _dialog = _dialogInstance.GetComponent<Dialogo>();
    }

    public override void Cancel()
    {
        Destroy(_dialogInstance);
        IsDisabled = true;
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        _index++;
        if (_index < Dialogs.Length)
        {
            if (_index == 0&& TriggerOnStart != null)
            {
                TriggerOnStart.Trigger();
            }
        }
        else
        {
            if (TriggerOnFinish != null)
            {
                TriggerOnFinish.Trigger();
            }
            Cancel();
        }
    }

    protected void ShowDialog()
    {
        if (_index > 0)
        {
            if (Dialogs[_index - 1].TriggerOnHide != null)
            {
                Dialogs[_index - 1].TriggerOnHide.Trigger();
            }
        }
        _dialog.SetUp(dialogManager, Dialogs[_index]);
        if (Dialogs[_index].TriggerOnShow != null)
        {
            Dialogs[_index].TriggerOnShow.Trigger();
        }
    }

}
