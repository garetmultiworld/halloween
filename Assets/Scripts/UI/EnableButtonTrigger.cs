﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnableButtonTrigger : TriggerInterface
{

    public Button button;

    public override void Cancel()
    {
        button.interactable = false;
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        button.interactable = true;
    }

}
