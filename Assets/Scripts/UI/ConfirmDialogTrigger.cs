﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfirmDialogTrigger : TriggerInterface
{

    public string Dialog;
    public TriggerInterface OnYes;
    public TriggerInterface OnNo;

    public override void Cancel()
    {
        
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        ConfirmDialogManager.Instance.ShowConfirm(Dialog, OnYes, OnNo);
    }
}
