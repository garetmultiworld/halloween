﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FastForwardTrigger : TriggerInterface
{

    public float Amount;

    public override void Cancel()
    {
        
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        TimeManager.Instance.FastForward(Amount);
    }
}
