﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using System;

public class CountUpTrigger : TriggerInterface
{

    public float TimeMultiplier = 1;
    public float StartTime=0;
    public Text DayText;
    public Text HourText;
    public Text MinuteText;
    public Text SecondText;
    public Text MilisecondText;
    public GameObject SecondHand;
    public GameObject MinuteHand;
    public GameObject HourHand;
    public bool DisplayInSeconds;

    [HideInInspector]
    public float CurrentTime;

    protected bool IsPlaying = false;

    private IEnumerator coroutine=null;

    public void Initialize(TimeManager timeManager)
    {
        StartTime = timeManager.StartTime;
        DayText = timeManager.DayText;
        HourText = timeManager.HourText;
        MinuteText = timeManager.MinuteText;
        SecondText = timeManager.SecondText;
        MilisecondText = timeManager.MilisecondText;
        SecondHand = timeManager.SecondHand;
        MinuteHand = timeManager.MinuteHand;
        HourHand = timeManager.HourHand;
        TimeMultiplier = timeManager.TimeMultiplier;
    }

    public override void Cancel()
    {
        IsPlaying = false;
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        IsPlaying = true;
        SetLabel(StartTime);
        if (coroutine != null)
        {
            StopCoroutine(coroutine);
        }
        coroutine = CountUp();
        StartCoroutine(coroutine);
    }

    private void SetLabelSeconds(float time)
    {
        if (SecondText != null)
        {
            int seconds = (int)Math.Floor(time);
            if (seconds < 10)
            {
                SecondText.text = "0" + seconds.ToString();
            }
            else
            {
                SecondText.text = seconds.ToString();
            }
        }
    }

    private void SetLabel(float time)
    {
        if (DisplayInSeconds)
        {
            SetLabelSeconds(time);
            return;
        }
        float remainingTime = time;
        int days = (int)Math.Floor(remainingTime / 86400);
        remainingTime -= days * 86400;
        if (DayText != null)
        {
            DayText.text = days.ToString();
        }
        int hours = (int)Math.Floor(remainingTime / 3600);
        remainingTime -= hours * 3600;
        if (HourText != null)
        {
            if (hours < 10)
            {
                HourText.text = "0" + hours.ToString();
            }
            else
            {
                HourText.text = hours.ToString();
            }
        }
        if (HourHand != null)
        {
            HourHand.transform.eulerAngles = new Vector3(
                0,
                0,
                -6 * hours
            );
        }
        int minutes = (int)Math.Floor(remainingTime / 60);
        remainingTime -= minutes * 60;
        if (MinuteText != null)
        {
            if (minutes < 10)
            {
                MinuteText.text = "0" + minutes.ToString();
            }
            else
            {
                MinuteText.text = minutes.ToString();
            }
        }
        if (MinuteHand != null)
        {
            MinuteHand.transform.eulerAngles = new Vector3(
                0,
                0,
                -6 * minutes
            );
        }
        int seconds = (int)Math.Floor(remainingTime);
        if (SecondText != null)
        {
            if (seconds < 10)
            {
                SecondText.text = "0" + seconds.ToString();
            }
            else
            {
                SecondText.text = seconds.ToString();
            }
            
        }
        if (SecondHand != null)
        {
            SecondHand.transform.eulerAngles = new Vector3(
                0,
                0,
                -6*seconds
            );
        }
        if (MilisecondText != null)
        {
            int miliseconds = ((int)Math.Floor(time * 100)) - (((int)Math.Floor(time)) * 100);
            if (miliseconds < 10)
            {
                MilisecondText.text = "0" + miliseconds.ToString();
            }
            else
            {
                MilisecondText.text = miliseconds.ToString();
            }
        }
    }

    private IEnumerator CountUp()
    {
        for (CurrentTime = StartTime; IsPlaying; CurrentTime += (Time.deltaTime* TimeMultiplier))
        {
            SetLabel(CurrentTime);
            yield return null;
        }
    }
}
