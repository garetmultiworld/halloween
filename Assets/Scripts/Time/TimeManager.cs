﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeManager : MonoBehaviour
{

    #region Static Instance
    private static TimeManager instance;
    public static TimeManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<TimeManager>();
                if (instance == null)
                {
                    instance = new GameObject("Spawned TimeManager", typeof(TimeManager)).GetComponent<TimeManager>();
                }
            }
            return instance;
        }
        private set
        {
            instance = value;
        }
    }
    #endregion

    public float TimeMultiplier = 1;
    public float StartTime = 0;
    public Text DayText;
    public Text HourText;
    public Text MinuteText;
    public Text SecondText;
    public Text MilisecondText;
    public GameObject SecondHand;
    public GameObject MinuteHand;
    public GameObject HourHand;
    public float TotalTime = 259200;
    public Slider TotalTimeSlider;
    public TriggerInterface TotalTimeTrigger;

    public GameObject IconDay1;
    public TriggerInterface Day1Trigger;
    public GameObject IconDay2;
    public TriggerInterface Day2Trigger;
    public GameObject IconDay3;

    public TimeManagerEvent[] Events;

    protected CountUpTrigger counter;
    protected CountDownTrigger finalCounter;
    protected List<CountDownTrigger> countDownTriggers=new List<CountDownTrigger>();

    protected bool _started=false;

    TimeManager()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    void Start()
    {
        _started = true;
        Initialize();
    }

    private void InitCounter()
    {
        if (counter == null)
        {
            counter = gameObject.AddComponent(typeof(CountUpTrigger)) as CountUpTrigger;
            counter.name = "TimeManager countup";
            counter.Initialize(this);
            finalCounter = gameObject.AddComponent(typeof(CountDownTrigger)) as CountDownTrigger;
            finalCounter.slider = TotalTimeSlider;
            finalCounter.TotalTime = TotalTime;
            finalCounter.TimeMultiplier = TimeMultiplier;
            finalCounter.OnFinish = TotalTimeTrigger;
        }
    }

    public float GetCurrentTime()
    {
        InitCounter();
        return counter.CurrentTime;
    }

    public void FastForward(float amount)
    {
        InitCounter();
        StartTime = GetCurrentTime()+(amount* TimeMultiplier);
        counter.StartTime = StartTime;
        finalCounter.StartTime = StartTime;
        Initialize();
    }

    public void Initialize()
    {
        InitCounter();
        foreach(CountDownTrigger countDown in countDownTriggers)
        {
            countDown.Cancel();
            Destroy(countDown);
        }
        countDownTriggers.Clear();
        counter.Cancel();
        if (!_started)
        {
            return;
        }
        counter.Trigger();
        finalCounter.Trigger();
        float totalTime;
        if (Events != null)
        {
            foreach (TimeManagerEvent evento in Events)
            {
                if (evento.Trigger == null)
                {
                    continue;
                }
                totalTime = evento.seconds + (evento.minutes * 60) + (evento.hours * 3600) + (evento.days * 86400) - StartTime;
                StartEvent(totalTime,evento.Trigger);
            }
        }
        DisableObjectTrigger disableObjectTrigger;
        GroupTrigger groupTrigger;
        totalTime = 86400 - StartTime;
        if (totalTime <= 0)
        {
            IconDay1.SetActive(false);
            if (Day1Trigger != null)
            {
                Day1Trigger.Trigger();
            }
        }
        else
        {
            groupTrigger = gameObject.AddComponent(typeof(GroupTrigger)) as GroupTrigger;
            groupTrigger.Triggers = new TriggerInterface[2];
            disableObjectTrigger = gameObject.AddComponent(typeof(DisableObjectTrigger)) as DisableObjectTrigger;
            disableObjectTrigger.TheObject = IconDay1;
            groupTrigger.Triggers[0] = disableObjectTrigger;
            groupTrigger.Triggers[1] = Day1Trigger;
            StartEvent(totalTime, groupTrigger);
        }
        totalTime = 172800 - StartTime;
        if (totalTime <= 0)
        {
            IconDay2.SetActive(false);
            if (Day2Trigger != null)
            {
                Day2Trigger.Trigger();
            }
        }
        else
        {
            groupTrigger = gameObject.AddComponent(typeof(GroupTrigger)) as GroupTrigger;
            groupTrigger.Triggers = new TriggerInterface[2];
            disableObjectTrigger = gameObject.AddComponent(typeof(DisableObjectTrigger)) as DisableObjectTrigger;
            disableObjectTrigger.TheObject = IconDay2;
            groupTrigger.Triggers[0] = disableObjectTrigger;
            groupTrigger.Triggers[1] = Day2Trigger;
            StartEvent(totalTime, groupTrigger);
        }
        totalTime = 259200 - StartTime;
        if (totalTime <= 0)
        {
            IconDay3.SetActive(false);
        }
        else
        {
            disableObjectTrigger = gameObject.AddComponent(typeof(DisableObjectTrigger)) as DisableObjectTrigger;
            disableObjectTrigger.TheObject = IconDay3;
            StartEvent(totalTime, disableObjectTrigger);
        }
    }

    private void StartEvent(float totalTime, TriggerInterface trigger)
    {
        if (totalTime < 0)
        {
            return;
        }
        CountDownTrigger countDownTrigger;
        countDownTrigger = gameObject.AddComponent(typeof(CountDownTrigger)) as CountDownTrigger;
        countDownTrigger.TotalTime = totalTime;
        countDownTrigger.TimeMultiplier = TimeMultiplier;
        countDownTrigger.OnFinish = trigger;
        countDownTrigger.Trigger();
        countDownTriggers.Add(countDownTrigger);
    }

    public void ClearData()
    {
        InitCounter();
        StartTime = 0;
        counter.StartTime = StartTime;
        finalCounter.StartTime = StartTime;
        Initialize();
    }

    public void LoadData(PlayerData playerData)
    {
        InitCounter();
        StartTime = playerData.CurrentTime;
        counter.StartTime = StartTime;
        finalCounter.StartTime = StartTime;
        Initialize();
    }

}
