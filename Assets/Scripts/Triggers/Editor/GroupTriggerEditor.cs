﻿using System;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GroupTrigger),true)]
public class GroupTriggerEditor : TriggerInterfaceEditor
{

    protected void SetUpPrefabConflict(GroupTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "GroupTrigger");
    }

    protected void StorePrefabConflict(GroupTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        _hadChanges = false;
        GroupTrigger groupTrigger=(GroupTrigger)target;
        SetUpPrefabConflict(groupTrigger);
        groupTrigger.showGroupTrigger = EditorGUILayout.Foldout(
            groupTrigger.showGroupTrigger,
            "Triggers ("+ groupTrigger.Triggers.Length.ToString()+")"
        );
        if (groupTrigger.showGroupTrigger)
        {
            if (groupTrigger.Triggers.Length == 0)
            {
                if(GUILayout.Button(EditorGUIUtility.IconContent("CreateAddNew"), GUILayout.Width(32)))
                {
                    Array.Resize(ref groupTrigger.Triggers, groupTrigger.Triggers.Length + 1);
                    _hadChanges = true;
                }
            }
            for (int iTrigger=0;iTrigger<groupTrigger.Triggers.Length;iTrigger++)
            {
                DrawItem(groupTrigger,iTrigger);
            }
        }
        StorePrefabConflict(groupTrigger);
    }

    protected void DrawItem(GroupTrigger groupTrigger, int iTrigger)
    {
        bool deleted = false;
        GUILayout.BeginHorizontal();
        if (GUILayout.Button(EditorGUIUtility.IconContent("Toolbar Minus"), GUILayout.Width(32)))
        {
            EditorArrayUtils.RemoveAt(ref groupTrigger.Triggers, iTrigger);
            deleted = true;
            _hadChanges = true;
        }
        if (GUILayout.Button(new GUIContent("Before", EditorGUIUtility.IconContent("Toolbar Plus").image)))
        {
            EditorArrayUtils.InsertBefore(ref groupTrigger.Triggers, iTrigger);
            _hadChanges = true;
        }
        if (GUILayout.Button(new GUIContent("After", EditorGUIUtility.IconContent("Toolbar Plus").image)))
        {
            EditorArrayUtils.InsertAfter(ref groupTrigger.Triggers, iTrigger);
            _hadChanges = true;
        }
        if (iTrigger == 0)
        {
            GUILayout.Label(" ", GUILayout.Width(32));
        }
        else
        {
            if (GUILayout.Button(EditorGUIUtility.IconContent("scrollup"), GUILayout.Width(32)))
            {
                EditorArrayUtils.MoveUp(ref groupTrigger.Triggers, iTrigger);
                _hadChanges = true;
            }
        }
        if (iTrigger == (groupTrigger.Triggers.Length - 1))
        {
            GUILayout.Label(" ", GUILayout.Width(32));
        }
        else
        {
            if (GUILayout.Button(EditorGUIUtility.IconContent("scrolldown"), GUILayout.Width(32)))
            {
                EditorArrayUtils.MoveDown(ref groupTrigger.Triggers, iTrigger);
                _hadChanges = true;
            }
        }
        GUILayout.EndHorizontal();
        if (!deleted)
        {
            DrawItemDetails(groupTrigger, iTrigger);
        }
    }

    protected void DrawItemDetails(GroupTrigger groupTrigger, int iTrigger)
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label(iTrigger.ToString());
        TriggerInterface prevTrigger = groupTrigger.Triggers[iTrigger];
        groupTrigger.Triggers[iTrigger] = (TriggerInterface)EditorGUILayout.ObjectField(
            groupTrigger.Triggers[iTrigger],
            typeof(TriggerInterface),
            true
        );
        if(prevTrigger!= groupTrigger.Triggers[iTrigger])
        {
            _hadChanges = true;
        }
        GUILayout.EndHorizontal();
        if (groupTrigger.Triggers[iTrigger] != null)
        {
            string desc = groupTrigger.Triggers[iTrigger].GetDescription();
            if (string.IsNullOrEmpty(desc))
            {
                desc = "(trigger has no description)";
            }
            EditorGUILayout.HelpBox(desc, MessageType.None);
        }
        if (iTrigger < (groupTrigger.Triggers.Length - 1))
        {
            EditorUtils.DrawUILine(Color.grey);
        }
    }

}
