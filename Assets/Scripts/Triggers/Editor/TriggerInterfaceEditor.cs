﻿using UnityEditor;
using UnityEngine;

public class TriggerInterfaceEditor : Editor
{

    protected bool _hadChanges = false;

    protected void SetUpPrefabConflict(TriggerInterface trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "TriggerInterface");
    }

    protected void StorePrefabConflict(TriggerInterface trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        _hadChanges = false;
        TriggerInterface triggerInterface = (TriggerInterface)target;
        SetUpPrefabConflict(triggerInterface);
        triggerInterface.showTriggerInterface = EditorGUILayout.Foldout(
            triggerInterface.showTriggerInterface, 
            "General ("+triggerInterface.GetLabel()+")"
        );
        if (triggerInterface.showTriggerInterface)
        {
            bool prevBool= triggerInterface.IsDisabled;
            triggerInterface.IsDisabled = EditorUtils.Checkbox("Is Disabled", triggerInterface.IsDisabled);
            if (prevBool!= triggerInterface.IsDisabled)
            {
                _hadChanges = true;
            }
            string prevString= triggerInterface.name;
            triggerInterface.name=EditorGUILayout.TextField("Name", triggerInterface.name);
            if (
                (string.IsNullOrEmpty(prevString) && !string.IsNullOrEmpty(triggerInterface.name)) ||
                (!string.IsNullOrEmpty(prevString) && string.IsNullOrEmpty(triggerInterface.name)) ||
                (prevString != null && !prevString.Equals(triggerInterface.name))
            )
            {
                _hadChanges = true;
            }
            int prevInt = triggerInterface.MaxActivationTimes;
            triggerInterface.MaxActivationTimes=EditorGUILayout.IntField(
                new GUIContent(
                    "Max Activation Times",
                    "The maximum amount of times this trigger can be activated, 0 or negative for unlimited"
                ),
                triggerInterface.MaxActivationTimes
            );
            if (prevInt!= triggerInterface.MaxActivationTimes)
            {
                _hadChanges = true;
            }
        }
        StorePrefabConflict(triggerInterface);
    }
}
