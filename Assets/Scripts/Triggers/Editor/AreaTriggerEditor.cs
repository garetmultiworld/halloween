﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(AreaTrigger))]
public class AreaTriggerEditor : Editor
{

    protected bool _hadChanges = false;

    protected void SetUpPrefabConflict(AreaTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "AreaTrigger");
    }

    protected void StorePrefabConflict(AreaTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        AreaTrigger areaTrigger = (AreaTrigger)target;
        SetUpPrefabConflict(areaTrigger);
        bool hasColliders = false;
        Collider2D []colliders = areaTrigger.GetComponents<Collider2D>();
        foreach(Collider2D collider in colliders)
        {
            if (collider.isTrigger)
            {
                hasColliders = true;
            }
        }
        if (!hasColliders)
        {
            EditorGUILayout.HelpBox("This object must have at least one Collider2D with trigger enabled", MessageType.Warning);
            if (GUILayout.Button("Add BoxCollider2D"))
            {
                BoxCollider2D box = areaTrigger.gameObject.AddComponent(typeof(BoxCollider2D)) as BoxCollider2D;
                box.isTrigger = true;
                _hadChanges = true;
            }
            if (GUILayout.Button("Add CircleCollider2D"))
            {
                CircleCollider2D circle = areaTrigger.gameObject.AddComponent(typeof(CircleCollider2D)) as CircleCollider2D;
                circle.isTrigger = true;
                _hadChanges = true;
            }
        }

        LayerMask prevLayerMask= areaTrigger.FilterLayer;
        areaTrigger.FilterLayer=EditorUtils.SelectLayerMask("Detectable Layers", areaTrigger.FilterLayer);
        if (prevLayerMask != areaTrigger.FilterLayer)
        {
            _hadChanges = true;
        }
        GUILayout.Label(" ");
        TriggerInterface prevTrigger = areaTrigger.OnEnter;
        areaTrigger.OnEnter = EditorUtils.SelectTrigger("On Enter", areaTrigger.OnEnter);
        if (prevTrigger != areaTrigger.OnEnter)
        {
            _hadChanges = true;
        }
        bool prevBool = areaTrigger.EnterAndExitExclusive;
        areaTrigger.EnterAndExitExclusive = EditorUtils.Checkbox(
            "Enter And Exit Cancel Each Other?", 
            areaTrigger.EnterAndExitExclusive
        );
        if(prevBool!= areaTrigger.EnterAndExitExclusive)
        {
            _hadChanges = true;
        }
        prevTrigger = areaTrigger.OnExit;
        areaTrigger.OnExit = EditorUtils.SelectTrigger("On Exit", areaTrigger.OnExit);
        if(prevTrigger!= areaTrigger.OnExit)
        {
            _hadChanges = true;
        }

        EditorGUILayout.HelpBox("If you want to save the object that enters or exits this area you can save it on its respective target holder.", MessageType.Info);
        TargetHolder prevHolder = areaTrigger.TargetOnEnter;
        areaTrigger.TargetOnEnter = (TargetHolder)EditorGUILayout.ObjectField(
            "On Enter",
            areaTrigger.TargetOnEnter,
            typeof(TargetHolder),
            true
        );
        if(prevHolder!= areaTrigger.TargetOnEnter)
        {
            _hadChanges = true;
        }
        prevHolder = areaTrigger.TargetOnExit;
        areaTrigger.TargetOnExit = (TargetHolder)EditorGUILayout.ObjectField(
            "On Exit",
            areaTrigger.TargetOnExit,
            typeof(TargetHolder),
            true
        );
        if(prevHolder!= areaTrigger.TargetOnExit)
        {
            _hadChanges = true;
        }
        StorePrefabConflict(areaTrigger);
    }

}
