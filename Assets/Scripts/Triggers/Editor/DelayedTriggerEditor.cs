﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(DelayedTrigger), true)]
public class DelayedTriggerEditor : TriggerInterfaceEditor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        DelayedTrigger delayedTrigger = (DelayedTrigger)target;
        delayedTrigger.showDelayedTrigger = EditorGUILayout.Foldout(
            delayedTrigger.showDelayedTrigger,
            "Delayed"
        );
        if (delayedTrigger.showDelayedTrigger)
        {
            delayedTrigger.TriggerToFire = EditorUtils.SelectTrigger("Trigger To Fire", delayedTrigger.TriggerToFire);
            delayedTrigger.Interval = EditorGUILayout.FloatField(new GUIContent("Interval", "The time before the trigger is fired (in seconds)."), delayedTrigger.Interval);
            if (delayedTrigger.Interval < 0)
            {
                delayedTrigger.Interval = 0;
            }
            delayedTrigger.Loop = EditorUtils.Checkbox("Loop", delayedTrigger.Loop);
            if (!delayedTrigger.Loop)
            {
                delayedTrigger.Times=EditorGUILayout.IntField("Times",delayedTrigger.Times);
            }
            if (delayedTrigger.Times < 0)
            {
                delayedTrigger.Times = 0;
            }
        }

    }
}
