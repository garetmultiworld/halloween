﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(OnStartTrigger))]
public class OnStartTriggerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        OnStartTrigger onStartTrigger = (OnStartTrigger)target;
        onStartTrigger.Trigger = EditorUtils.SelectTrigger("On Enter", onStartTrigger.Trigger);
    }
}
