﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CancelTrigger), true)]
public class CancelTriggerEditor : TriggerInterfaceEditor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        CancelTrigger cancelTrigger = (CancelTrigger)target;
        cancelTrigger.TriggerToCancel = EditorUtils.SelectTrigger("Trigger To Cancel", cancelTrigger.TriggerToCancel);
    }
}
