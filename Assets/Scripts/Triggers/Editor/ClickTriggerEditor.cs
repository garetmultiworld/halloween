﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ClickTrigger))]
public class ClickTriggerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        EditorGUILayout.HelpBox("This uses Unity's OnMouseDown method, if the trigger isn't firing there's probably some collider or UI interference.", MessageType.Info);

        ClickTrigger clickTrigger = (ClickTrigger)target;

        clickTrigger.Trigger = EditorUtils.SelectTrigger("On Enter", clickTrigger.Trigger);
    }
}
