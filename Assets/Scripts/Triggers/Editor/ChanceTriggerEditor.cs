﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ChanceTrigger), true)]
public class ChanceTriggerEditor : TriggerInterfaceEditor
{

    protected void SetUpPrefabConflict(ChanceTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "ChanceTrigger");
    }

    protected void StorePrefabConflict(ChanceTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        _hadChanges = false;
        ChanceTrigger chanceTrigger = (ChanceTrigger)target;
        chanceTrigger.showChanceTrigger = EditorGUILayout.Foldout(
            chanceTrigger.showChanceTrigger,
            "Chance"
        );
        if (chanceTrigger.showChanceTrigger)
        {
            TriggerInterface prevTrigger = chanceTrigger.TriggerToFire;
            chanceTrigger.TriggerToFire = EditorUtils.SelectTrigger("Trigger To Fire", chanceTrigger.TriggerToFire);
            if (prevTrigger != chanceTrigger.TriggerToFire)
            {
                _hadChanges = true;
            }
            float prevFloat = chanceTrigger.Chances;
            chanceTrigger.Chances = EditorGUILayout.Slider("Chances",chanceTrigger.Chances, 0f, 100f);
            if (prevFloat != chanceTrigger.Chances)
            {
                _hadChanges = true;
            }
        }
            
    }

}
