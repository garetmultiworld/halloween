﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleTrigger : TriggerInterface
{

    public TriggerInterface TriggerA;
    public TriggerInterface TriggerB;
    public bool FiresB=false;

    public override void Cancel()
    {

    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (FiresB)
        {
            TriggerB.Trigger();
            FiresB = false;
        }
        else
        {
            TriggerA.Trigger();
            FiresB = true;
        }
    }
}
