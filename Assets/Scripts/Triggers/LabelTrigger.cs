﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LabelTrigger : MonoBehaviour
{

    public GameObject Parent;

    public void Trigger(string label)
    {
        TriggerInterface[] Triggers;
        if (Parent != null)
        {
            Triggers = Parent.GetComponents<TriggerInterface>();
        }
        else
        {
            Triggers = this.gameObject.GetComponents<TriggerInterface>();
        }
        foreach (TriggerInterface trigger in Triggers)
        {
            if (label.Equals(trigger.GetLabel()))
            {
                trigger.Trigger();
            }
        }
    }

}
