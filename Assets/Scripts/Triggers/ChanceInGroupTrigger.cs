﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChanceInGroupTrigger : TriggerInterface
{

    public ChanceGroupTriggerItem[] Items;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        float chance;
        float currentChance = 0f;
        for (int i = 0; i < Items.Length; i++)
        {
            chance = Random.Range(0f, 100f);
            if (chance <= (Items[i].Chances + currentChance))
            {
                Items[i].Trigger();
                return;
            }
            currentChance += Items[i].Chances;
        }
    }
}
