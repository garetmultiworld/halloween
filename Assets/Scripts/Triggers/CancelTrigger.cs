﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CancelTrigger : TriggerInterface
{

#if UNITY_EDITOR
    public bool showCancelTrigger = true;
#endif

    public TriggerInterface TriggerToCancel;

    public override void Cancel()
    {}

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        TriggerToCancel.Cancel();
    }

    public override string GetDescription()
    {
        string desc = base.GetDescription();
        if (TriggerToCancel==null)
        {
            desc += " (no trigger assigned)";
        }
        else
        {
            desc += " ("+TriggerToCancel.GetLabel()+")";
        }
        return desc;
    }

}
