﻿using System.Collections;
using UnityEngine;

public class DelayedTrigger : TriggerInterface
{

#if UNITY_EDITOR
    public bool showDelayedTrigger = true;
#endif

    public TriggerInterface TriggerToFire;
    public float Interval=1;
    public bool Loop;
    public int Times=1;
    public TriggerInterface TriggerOnLoopEnd;

    protected bool IsPlaying = false;

    public override void Cancel()
    {
        IsPlaying = false;
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        IsPlaying = true;
        StartCoroutine(DelayedFire());
    }

    private IEnumerator DelayedFire()
    {
        float count;
        for (count = 0; (Loop || count < Times) && IsPlaying; count++)
        {
            float time;
            for (time = 0; time < Interval && IsPlaying; time += Time.deltaTime)
            {
                yield return null;
            }
            if (IsPlaying)
            {
                TriggerToFire.Trigger();
            }
        }
        if (TriggerOnLoopEnd != null)
        {
            TriggerOnLoopEnd.Trigger();
        }
    }

    public override string GetDescription()
    {
        string desc = base.GetDescription();
        if (TriggerToFire == null)
        {
            desc += " (no trigger assigned)";
        }
        else
        {
            desc += ", " + Interval.ToString() + "s";
            if (Loop)
            {
                desc += ", loop";
            }
            else
            {
                desc += ", " + Times.ToString() + " times";
            }
            desc += " (" + TriggerToFire.GetLabel() + ")";
        }
        return desc;
    }

}
