﻿using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CustomEditor(typeof(GenericEventListener))]
public class GenericEventListenerEditor : Editor
{
    protected bool _hadChanges = false;

    protected void SetUpPrefabConflict(GenericEventListener genericEventListener)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(genericEventListener, "GenericEventListener");
    }

    protected void StorePrefabConflict(GenericEventListener genericEventListener)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(genericEventListener);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(genericEventListener.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        GenericEventListener genericEventListener = (GenericEventListener)target;
        SetUpPrefabConflict(genericEventListener);
        string prevString = genericEventListener.EventName;
        genericEventListener.EventName = EditorGUILayout.TextField("Event Name", genericEventListener.EventName);
        if (
            (string.IsNullOrEmpty(prevString) && !string.IsNullOrEmpty(genericEventListener.EventName)) ||
            (!string.IsNullOrEmpty(prevString) && string.IsNullOrEmpty(genericEventListener.EventName)) ||
            (prevString != null && !prevString.Equals(genericEventListener.EventName))
        )
        {
            _hadChanges = true;
        }
        TriggerInterface prevTrigger = genericEventListener.OnEvent;
        genericEventListener.OnEvent = EditorUtils.SelectTrigger("On Event", genericEventListener.OnEvent);
        if (prevTrigger != genericEventListener.OnEvent)
        {
            _hadChanges = true;
        }
        StorePrefabConflict(genericEventListener);
    }
}
