﻿using UnityEngine;

public class SetEventNumberFromDataTrigger : TriggerInterface
{

    public GenericEventTrigger genericEventTrigger;
    public GameObject Holder;
    public bool FromTarget;
    public string LabelData;

    public override void Cancel()
    {

    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        DataHolder[] dataHolders;
        GameObject tempHolder;
        if (Holder != null)
        {
            tempHolder = Holder;
        }
        else
        {
            tempHolder = this.gameObject;
        }
        if (FromTarget)
        {
            tempHolder = tempHolder.GetComponent<TargetHolder>().Target;
        }
        dataHolders = tempHolder.GetComponents<DataHolder>();
        foreach (DataHolder data in dataHolders)
        {
            if (LabelData.Equals(data.Name))
            {
                switch (data.type)
                {
                    default:
                        //not supported
                        break;
                    case DataHolder.Type.Number:
                        genericEventTrigger.EventNumber = data.FloatValue;
                        break;
                    case DataHolder.Type.Integer:
                        genericEventTrigger.EventNumber = data.IntValue;
                        break;
                    case DataHolder.Type.Bool:
                        //not supported
                        break;
                }
                break;
            }
        }
    }

}
