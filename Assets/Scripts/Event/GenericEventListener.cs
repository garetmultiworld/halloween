﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericEventListener : MonoBehaviour
{

    public string EventName;
    public TriggerInterface OnEvent;

    private bool _registered = false;

    void Awake()
    {
        Register();
    }

    public void Register()
    {
        if (_registered)
        {
            return;
        }
        _registered = GenericEventManager.Instance.RegisterListener(this);
    }

    public void TriggerStringEvent(GameObject source, string EventParameter, float EventNumber)
    {
        if (!gameObject.activeSelf)
        {
            return;
        }
        if (OnEvent!= null)
        {
            OnEvent.StringParameter = EventParameter;
            OnEvent.FloatParameter = EventNumber;
            OnEvent.EventSource = source;
            OnEvent.Trigger();
        }
    }

}
