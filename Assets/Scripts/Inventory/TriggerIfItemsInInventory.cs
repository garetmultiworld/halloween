﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerIfItemsInInventory : TriggerInterface
{

    public Inventory inventory;
    public InventoryItem[] items;
    public TriggerInterface IfTrue;
    public TriggerInterface IfFalse;

    protected Inventory realInventory;

    public override void Cancel()
    {
        
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (realInventory == null)
        {
            realInventory = InventoryManager.Instance.getInventory(inventory);
        }
        if (realInventory.HasItems(items))
        {
            if (IfTrue != null)
            {
                IfTrue.Trigger();
            }
        }
        else
        {
            if (IfFalse != null)
            {
                IfFalse.Trigger();
            }
        }
    }
}
