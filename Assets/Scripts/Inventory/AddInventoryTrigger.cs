﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddInventoryTrigger : TriggerInterface
{

    public Inventory inventory;
    public string Item;
    public int Amount;

    protected Inventory realInventory;

    void Start()
    {
        if (realInventory == null)
        {
            realInventory = InventoryManager.Instance.getInventory(inventory);
        }
    }

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        realInventory.AddItem(Item, Amount);
    }

}
