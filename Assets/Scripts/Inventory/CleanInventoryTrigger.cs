﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CleanInventoryTrigger : TriggerInterface
{

    public Inventory inventory;

    protected Inventory realInventory;

    void Start()
    {
        if (realInventory == null)
        {
            realInventory = InventoryManager.Instance.getInventory(inventory);
        }
    }

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        realInventory.Clean();
    }
}
