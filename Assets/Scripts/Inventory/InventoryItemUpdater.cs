﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class InventoryItemUpdater:MonoBehaviour
{

    public string ItemName;
    public Slider slider;
    public TriggerInterface OnChange;

    void Start()
    {
        StartCoroutine(DelayedFire());
    }

    private IEnumerator DelayedFire()
    {
        float count;
        for (count = 0; count<1; count+=Time.deltaTime)
        {
            yield return null;
        }
        InventoryManager.Instance.RegisterUpdater(this);
    }

}
