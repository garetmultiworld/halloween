﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateTextInventoryAmountTrigger : TriggerInterface
{

    public Inventory inventory;
    public string ItemName;
    public Text text;
    public string Prefix;
    public string Suffix;

    protected Inventory realInventory;

    public override void Cancel()
    {
        
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (realInventory == null)
        {
            realInventory = InventoryManager.Instance.getInventory(inventory);
        }
        text.text= Prefix + realInventory.GetItemAmount(ItemName)+ Suffix;
    }

    
}
