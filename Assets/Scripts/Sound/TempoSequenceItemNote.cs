﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TempoSequenceItemNote
{
    
    public enum Type
    {
        Semifusa,
        Fusa,
        Semicorchea,
        Corchea,
        Negra,
        Blanca,
        Redonda
    }

    public Type type=Type.Negra;
    public int amount = 1;
    public bool IsSilence;

}
