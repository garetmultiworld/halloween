﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicInfoTrigger : TriggerInterface
{

    public MusicInfo musicInfo;
    public TriggerInterface TriggerOnNote;
    public TriggerInterface TriggerOnSilence;
    public TriggerInterface TriggerOnFinish;

    protected bool IsAnimating;
    protected int SequenceIndex;
    protected int SequenceNoteIndex;
    protected TempoSequence tempoSequence;
    protected TempoSequenceItem currentSequence;
    protected TempoSequenceItemNote currentNote;
    protected float semifusa;
    protected float fusa;
    protected float semicorchea;
    protected float corchea;
    protected float negra;
    protected float blanca;
    protected float redonda;
    protected float MaxTimer;
    protected float CurrentTimer;
    protected int NoteCounter;

    public override void Cancel()
    {
        IsAnimating = false;
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        IsAnimating = true;
        SequenceIndex = 0;
        SequenceNoteIndex = -1;
        tempoSequence = musicInfo.tempoSequence;
        currentSequence = tempoSequence.Items[SequenceIndex];
        UpdateTempo();
        NextNote();
        GetNextMaxTime();
    }

    protected void NextItem()
    {
        SequenceIndex++;
        if (SequenceIndex >= tempoSequence.Items.Length)
        {
            if (musicInfo.Loop)
            {
                SequenceIndex = 0;
                currentSequence = tempoSequence.Items[SequenceIndex];
            }
            else
            {
                IsAnimating = false;
                if (TriggerOnFinish != null)
                {
                    TriggerOnFinish.Trigger();
                }
            }
        }
        else
        {
            currentSequence = tempoSequence.Items[SequenceIndex];
        }
    }

    protected void NextNote()
    {
        NoteCounter = -1;
        SequenceNoteIndex++;
        while (IsAnimating&&SequenceNoteIndex >= currentSequence.Notes.Length)
        {
            NextItem();
            SequenceNoteIndex = 0;
        }
        if (IsAnimating&&currentSequence.Notes[SequenceNoteIndex].amount == 0)
        {
            NextNote();
        }
        if (IsAnimating)
        {
            currentNote = currentSequence.Notes[SequenceNoteIndex];
        }
    }

    protected void GetNextMaxTime()
    {
        if (!IsAnimating)
        {
            return;
        }
        NoteCounter++;
        while (IsAnimating&&NoteCounter >= currentNote.amount)
        {
            NextNote();
            NoteCounter++;
        }
        if (!IsAnimating)
        {
            return;
        }
        CurrentTimer = 0;
        switch (currentNote.type)
        {
            case TempoSequenceItemNote.Type.Semifusa:
                MaxTimer = semifusa;
                break;
            case TempoSequenceItemNote.Type.Fusa:
                MaxTimer = fusa;
                break;
            case TempoSequenceItemNote.Type.Semicorchea:
                MaxTimer = semicorchea;
                break;
            case TempoSequenceItemNote.Type.Corchea:
                MaxTimer = corchea;
                break;
            case TempoSequenceItemNote.Type.Negra:
                MaxTimer = negra;
                break;
            case TempoSequenceItemNote.Type.Blanca:
                MaxTimer = blanca;
                break;
            case TempoSequenceItemNote.Type.Redonda:
                MaxTimer = redonda;
                break;
        }
    }

    protected void UpdateTempo()
    {
        negra = 60 / musicInfo.Tempo;
        semifusa= negra / 16;
        fusa= negra / 8;
        semicorchea = negra / 4;
        corchea = negra / 2;
        blanca = negra * 2;
        redonda= negra* 4;
    }

    void Update()
    {
        if (!IsAnimating)
        {
            return;
        }
        CurrentTimer += Time.deltaTime;
        while (IsAnimating&&CurrentTimer >= MaxTimer)
        {
            if (currentSequence.IsSilence||currentNote.IsSilence)
            {
                if (TriggerOnSilence != null)
                {
                    TriggerOnSilence.Trigger();
                }
            }
            else
            {
                if (TriggerOnNote != null)
                {
                    TriggerOnNote.Trigger();
                }
            }
            CurrentTimer -= MaxTimer;
            GetNextMaxTime();
            if (!IsAnimating)
            {
                return;
            }
        }
    }

}
