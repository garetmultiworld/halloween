﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayTrigger : TriggerInterface
{

    public string Music;
    public MusicPlayer.playTypes Type;
    public float TransitionTime = 1;
    public float WaitTime = 0;
    public float PlayFrom = 0;

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        switch (Type)
        {
            case MusicPlayer.playTypes.Play:
                AudioManager.Instance.PlayMusic(Music, PlayFrom);
                break;
            case MusicPlayer.playTypes.PlayWithFade:
                AudioManager.Instance.PlayMusicWithFade(Music, TransitionTime, WaitTime, PlayFrom);
                break;
            case MusicPlayer.playTypes.PlayWithCrossFade:
                AudioManager.Instance.PlayMusicWithCrossFade(Music, TransitionTime, PlayFrom);
                break;
        }
    }

    public override void Cancel()
    {
    }

}
