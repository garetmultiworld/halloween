﻿using System.Collections;
using UnityEngine;

public class MusicPlayer:MonoBehaviour
{
    public enum playTypes
    {
        Play,
        PlayWithFade,
        PlayWithCrossFade
    }

    public enum controlTypes
    {
        Stop,
        StopWithFade,
        Pause,
        Resume,
        Volume,
        GradualVolume
    }

    #region Fields
    private AudioSource musicSource;
    private AudioSource musicSource2;

    private string currentSound = "";

    [Range(0f, 1f)]
    public float volume=1f;
    public float volumeMultiplier;

    private bool firstMusicSourceIsPlaying;
    private bool isFading;
    #endregion

    private void Awake()
    {
        musicSource = this.gameObject.AddComponent<AudioSource>();
        musicSource2 = this.gameObject.AddComponent<AudioSource>();
    }

    protected void SetupAudioSource(AudioSource source,Sound sound, float from)
    {
        source.clip = sound.clip;
        source.pitch = sound.pitch;
        musicSource.loop = sound.loop;
        source.panStereo = sound.pan;
        source.volume = volume * sound.volume * volumeMultiplier;
        source.reverbZoneMix = sound.reverb;
        source.outputAudioMixerGroup = sound.audioMixerGroup;
        source.time = from;
    }

    public void Stop()
    {
        AudioSource activeSource = (firstMusicSourceIsPlaying) ? musicSource : musicSource2;
        activeSource.Stop();
    }

    public void StopWithFade(float transitionTime)
    {
        AudioSource activeSource = (firstMusicSourceIsPlaying) ? musicSource : musicSource2;
        UpdateStopMusicWithFade(activeSource, transitionTime);
    }

    public void Pause()
    {
        AudioSource activeSource = (firstMusicSourceIsPlaying) ? musicSource : musicSource2;
        activeSource.Pause();
    }

    public void Resume()
    {
        AudioSource activeSource = (firstMusicSourceIsPlaying) ? musicSource : musicSource2;
        activeSource.Play();
    }

    public void PlayMusic(Sound sound, float multiplier, float from)
    {
        if (sound.name.Equals(currentSound))
        {
            return;
        }
        currentSound = sound.name;
        AudioSource activeSource = (firstMusicSourceIsPlaying) ? musicSource : musicSource2;
        volumeMultiplier = multiplier;
        SetupAudioSource(activeSource, sound, from);
        activeSource.Play();
    }
    
    public void PlayMusicWithFade(Sound sound, float multiplier, float transitionTime,float waitTime, float from)
    {
        if (sound.name.Equals(currentSound))
        {
            return;
        }
        currentSound = sound.name;
        AudioSource activeSource = (firstMusicSourceIsPlaying) ? musicSource : musicSource2;
        volumeMultiplier = multiplier;
        StartCoroutine(UpdatePlayMusicWithFade(activeSource, sound, transitionTime, waitTime,from));
    }

    public void PlayMusicWithCrossFade(Sound sound, float multiplier, float transitionTime, float from)
    {
        if (sound.name.Equals(currentSound))
        {
            return;
        }
        currentSound = sound.name;
        AudioSource activeSource = (firstMusicSourceIsPlaying) ? musicSource : musicSource2;
        volumeMultiplier = multiplier;
        AudioSource newSource = (!firstMusicSourceIsPlaying) ? musicSource : musicSource2;

        firstMusicSourceIsPlaying = !firstMusicSourceIsPlaying;

        SetupAudioSource(newSource, sound, from);
        newSource.Play();
        StartCoroutine(UpdatePlayMusicWithCrossFade(activeSource, newSource, transitionTime));
    }

    private IEnumerator UpdateStopMusicWithFade(AudioSource activeSource, float transitionTime)
    {
        isFading = true;
        float t = 0;
        for (t = 0; t < transitionTime; t += Time.deltaTime)
        {
            activeSource.volume = (volume - ((t / transitionTime) * volume)) * volumeMultiplier;
            yield return null;
        }
        activeSource.Stop();
    }

    private IEnumerator UpdatePlayMusicWithFade(AudioSource activeSource, Sound newSound, float transitionTime, float waitTime, float from)
    {
        isFading = true;
        float t = 0;
        if (activeSource.isPlaying)
        {
            for (t = 0; t < transitionTime; t += Time.deltaTime)
            {
                activeSource.volume = (volume - ((t / transitionTime) * volume)) * volumeMultiplier;
                yield return null;
            }

            activeSource.Stop();
        }
        if (waitTime > 0)
        {
            yield return new WaitForSeconds(waitTime);
        }
        SetupAudioSource(activeSource, newSound, from);
        activeSource.Play();
        for (t = 0; t < transitionTime; t += Time.deltaTime)
        {
            activeSource.volume = (t / transitionTime) * volume* volumeMultiplier;
            yield return null;
        }
        isFading = false;
    }

    private IEnumerator UpdatePlayMusicWithCrossFade(AudioSource original, AudioSource newSource, float transitionTime)
    {
        isFading = true;

        float t = 0;

        for (t = 0; t < transitionTime; t += Time.deltaTime)
        {
            original.volume = (volume - ((t / transitionTime) * volume) * volumeMultiplier);
            newSource.volume = (t / transitionTime) * volume * volumeMultiplier;
            yield return null;
        }

        original.Stop();

        isFading = false;
    }

    public void SetVolumeMultiplier(float multiplier)
    {
        volumeMultiplier = multiplier;
        if (!isFading)
        {
            musicSource.volume = volume * volumeMultiplier;
            musicSource2.volume = volume * volumeMultiplier;
        }
    }

    public void SetVolume(float volume)
    {
        this.volume = volume;
        if (!isFading)
        {
            musicSource.volume = this.volume * volumeMultiplier;
            musicSource2.volume = this.volume * volumeMultiplier;
        }
    }

    public void SetMute(bool newMute)
    {
        musicSource.mute = newMute;
        musicSource2.mute = newMute;
    }

}
