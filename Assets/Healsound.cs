﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Healsound : MonoBehaviour
{
   public AK.Wwise.Event MyEvent;

   void SonidoCurar()
   {
       MyEvent.Post(gameObject);
   }
}
