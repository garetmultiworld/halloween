﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PostWwiseEvent4 : MonoBehaviour
{

    public string initialTag;
    public AK.Wwise.Event Musica;
    public AK.Wwise.Event Paramusica;
    // Start is called before the first frame update
    void Start()
    {
        Musica.Post(gameObject);
        SetTag(initialTag);
    }
    void OnTriggerEnter2D(Collider2D collider)
    {
        SetTag(collider.gameObject.tag);
    }  
       

    void SetTag(string newTag)
    {
        if (newTag == "CasaBruja")
        {
            AkSoundEngine.SetSwitch("Music", "CasaBruja", gameObject);
        }
        if (newTag == "Exterior")
        {
            AkSoundEngine.SetSwitch("Music", "Exterior", gameObject);
        }
        if(newTag == "Dungeon")
        {
            AkSoundEngine.SetSwitch("Music", "Dungeon", gameObject);
        }
    }

    void OnDestroy()
    {
        Paramusica.Post(gameObject);
    }

}
